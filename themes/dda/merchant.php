<?php
/**
 * Merchant page template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$title_img = $view->getThemePath().'/css/images/Merchant-Banner-Placeholder.jpg';
if ($header_img)
  $title_img = $header_img;
$google = null;
if ($c->getAttribute('merchant_google_places_json'))
  $google = json_decode($c->getAttribute('merchant_google_places_json'));
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
  .googleMapCanvas{ width:100%; border:0px none; height: 400px;}
  .googleMapCanvas img{max-width: none !important;}
</style>
<div class="container-fluid interior right-sidebar">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
      <div class="text-center">
        <div class="text-title">
          <?php
            $ti = new Area('Page Title');
            $ti->display($c);
          ?>
        </div>
      </div>
    </div>
  </div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 page-sub-header">
          <?php if ($st = $c->getAttribute('merchant_sub_title')) echo sprintf("<h2>%s</h2>", $st) ?>
          <?php if ($c->getCollectionDescription() != '' && $dtca_member) echo sprintf("<h4>%s</h4>", $c->getCollectionDescription()) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-content">
          <div class="row">
            <div class="col-xs-12 col-sm-5">
              <div class="district <?php if ($d = $c->getAttribute('merchant_district')) echo preg_replace('/\s/', '-', strtolower($d))?>"></div>
            <?php $fi = $c->getAttribute('merchant_feature_image');
              if ($dtca_member && is_object($fi)) { ?>
              <img class="img-responsive" src="<?php echo $fi->getRelativePath() ?>" />
            <?php } else { ?>
              <img class="img-responsive" src="<?php echo $view->getThemePath() . '/css/images/shopping_icons/' . $merchant_cat_class . '.png' ?>" />
            <?php } ?>
            <?php if ($dtca_member && isset($instagram_post)) {
              echo $instagram_post;
            } ?>
            </div>
            <div class="col-xs-12 col-sm-7">
              <h3>Visit Us</h3>
              <ul class="merchant-details">
              <?php if ($address) echo sprintf("<li class=\"address\"><span>%s</span></li>", $address) ?>
              <?php if ($phone) echo sprintf("<li class=\"phone\"><span>%s</span></li>", $phone) ?>
              <?php if ($dtca_member): ?>
                <?php if ($website) echo sprintf("<li class=\"website\"><span>%s</span></li>", $website) ?>
                <?php if ($merchant_facebook) echo sprintf("<li class=\"icon-facebook-blue\"><span><a href=\"%s\" target=\"_blank\">Facebook</a></span></li>", $merchant_facebook) ?>
                <?php if ($merchant_twitter) echo sprintf("<li class=\"icon-twitter-blue\"><span><a href=\"%s\" target=\"_blank\">Twitter</a></span></li>", $merchant_twitter) ?>
                <?php if ($merchant_instagram) echo sprintf("<li class=\"icon-instagram-blue\"><span><a href=\"%s\" target=\"_blank\">Instagram</a></span></li>", $merchant_instagram) ?>
              <?php endif; ?>
              </ul>
              <?php if ($google && $hours) { ?>
              <h3>Hours</h3>
              <div>
              <?php echo implode('<br/>',$hours) ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php
            $a = new Area('Addtional Content');
            $a->setAreaGridMaximumColumns(12);
            $a->display($c);
          ?>
        </div>
        <div class="col-md-4 hidden-sm hidden-xs col-sidebar">
          <div>
            <?php
              $sf = new GlobalArea('Sidebar Form');
              if ($sf->getTotalBlocksInArea($c) > 0) echo '<div class="sidebar-form">';
              $sf->display($c);
              if ($sf->getTotalBlocksInArea($c) > 0) echo '</div>';
            ?>
            <?php
              $sc = new GlobalArea('Sidebar Content');
              if ($sc->getTotalBlocksInArea() > 0) echo '<div class="sidebar-cta">';
              $sc->display($c);
              if ($sc->getTotalBlocksInArea() > 0) echo '</div>';
            ?>
          </div>
        </div>
      </div>
    </div> <!-- /container -->
  </main>
</div> <!-- /container-fluid -->
<?php if ($latitude && $longitude): ?>
<section id="map">
  <div class="container-fluid">
    <?php
      $a = new GlobalArea('Merchant Map');
      $a->display($c);
    ?>
  </div>
</section> <!-- /container-fluid -->
<?php endif; ?>
<?php $this->inc('elements/footer.php'); ?>

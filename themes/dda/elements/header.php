<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php');
?>

<header class="main-nav-header<?php if (!$c->isEditMode()) echo ' fix-to-top' ?>">
  <?php
    $a = new GlobalArea('Header Navigation');
    $a->display();
  ?>
</header>
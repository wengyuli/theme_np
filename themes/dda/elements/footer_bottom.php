<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
      <span id="top-link-block" class="hidden">
        <a href="#top" id="top-link-link">
        </a>
      </span>
    </div>
    <script src="<?php echo $view->getThemePath() ?>/js/bootstrap.min.js"></script>
    <?php Loader::element('footer_required'); ?>
    <script>
      $(document).ready(function(){
//        $(window).scroll(function(){
//          if ($(this).scrollTop() > 100) {
//            $('.navbar-brand').addClass('scrolled');
//          } else {
//            $('.navbar-brand').removeClass('scrolled');
//          }
//        });

        $('#top-link-link').click(function(){$('html,body').animate({scrollTop:0},'slow');return false;})

        if ( ($(window).height() + 120) < $(document).height() ) {
          $('#top-link-block').removeClass('hidden').affix({
            offset: {top:200}
          });
        }

        $('#header-search-submit').click(function(e){
          if ($('#header-search-query').hasClass('open')) {
            $('#header-search-query').css({'width': '0px'});
            $('#site-search').submit();
          }
          else {
            e.preventDefault();
            $('#header-search-query').css({'width': '300px'}).focus();
          }
        });
        $('#header-search-query').on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',function(){
          $('#header-search-query').toggleClass('open');
          $('#header-search-query').css({'width': ''});
        });
        $(document).keydown(function(e){
          if(e.keyCode == 27) {
            if ($('#header-search-query').hasClass('open')) {
              $('#header-search-query').css({'width': '0px'});
            }
          }
        });
      });
    </script>
  </body>
</html>
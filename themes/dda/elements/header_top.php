<?php defined('C5_EXECUTE') or die("Access Denied."); 
  global $u;
?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php echo $html->css($view->getStylesheet('main.less'))?>
	<link href="<?php echo $view->getStylesheet('font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $view->getStylesheet('print.less'); ?>" rel="stylesheet" type="text/css" media="print" />
  <?php Loader::element('header_required', array('pageTitle' => $pageTitle));?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://use.typekit.net/dnu7urb.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body>
<div class="<?php echo $c->getPageWrapperClass()?><?php if ($u->isLoggedIn()) echo ' authenticated' ?>" id="#dda">
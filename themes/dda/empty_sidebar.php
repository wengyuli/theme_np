<?php
/**
 * Right sidebar template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$title_img = $view->getThemePath().'/css/images/Banner-Placeholder.jpg';
$banner = $c->getAttribute('header_img');
if ($banner)
  $title_img = $banner->getRelativePath();
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
</style>
<div class="container-fluid interior right-sidebar">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
      <div class="text-center">
        <div class="text-title">
          <?php
            $ti = new Area('Page Title');
            $ti->display($c);
          ?>
        </div>
      </div>
    </div>
  </div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 page-sub-header">
        <?php
          $a = new Area('Page Header');
          $a->display($c);
        ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-content">
          <?php
            $a = new Area('Upper');
            $a->setAreaGridMaximumColumns(12);
            $a->display($c);
          ?>
        </div>
        <div class="col-md-4 hidden-sm hidden-xs col-sidebar">
          <div id="affixed-sidebar">
            <?php
              $sc = new Area('Sidebar Content');
              $sc->display($c);
            ?>
          </div>
        </div>
      </div>
    </div> <!-- /container -->
    <?php
      $a = new Area('Spotlight');
      if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0) {
    ?>
    <div class="container-fluid spotlight">
      <div class="row">
        <div class="col-xs-12">
          <div class="container">
            <div class="row">
              <div class="col-md-8">
    <?php } ?>
                <?php $a->display($c) ?>
    <?php if ($c->isEditMode() || $a->getTotalBlocksInArea($c) > 0) { ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-content">
          <?php
            $a = new Area('Lower');
            $a->setAreaGridMaximumColumns(12);
            $a->display($c);
          ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 visible-xs visible-sm col-sidebar">
          <div class="sidebar-small">
          <?php
            if (!$c->isEditMode()) {
              if ($sc->getTotalBlocksInArea() > 0) echo '<div class="sidebar-cta">';
              $sc->display($c);
              if ($sc->getTotalBlocksInArea() > 0) echo '</div>';
            }
          ?>
          </div>
        </div>
      </div>
    </div> <!-- /container -->
  </main>
</div> <!-- /container-fluid -->
<?php $this->inc('elements/footer.php'); ?>
<?php if (!$c->isEditMode()): ?>
<script>
  $(window).load(function () {
    if ($('.right-sidebar').innerHeight() > 1000) {
      $top = $('.main-nav-header').height() + $('#title-banner').height() + $('.page-sub-header').height();
      $('#affixed-sidebar').affix({
        offset: {
          top: $top,
          bottom: $('#visual-footer').height() + 50
        }
      });
    }
  });
</script>
<?php endif; ?>

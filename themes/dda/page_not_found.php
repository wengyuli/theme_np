<?php defined('C5_EXECUTE') or die("Access Denied.");
  $this->inc('elements/header.php');
  $title_img = $view->getThemePath().'/css/images/Banner-Placeholder.jpg';
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
</style>
<div class="container-fluid interior full">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
      <div class="text-center">
        <div class="text-title">
          <h1>
              <?php echo t('Page Not Found')?>
          </h1>
        </div>
      </div>
    </div>
  </div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <h2><?php echo t('Sorry, no page could be found at this address.')?></h2>
          <?php $a = new Area('Main'); ?>
          <?php $a->display($c); ?>
        </div>
      </div>
      <div class="row" style="padding: 30px 0px;">
        <div class="col-xs-12">
          <?php $a = new Area('Main'); ?>
          <?php $a->display($c); ?>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 text-center" style="margin-top: 100px;">
          <a href="<?php echo DIR_REL?>/"><?php echo t('Back to Home')?></a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
          <form action="/index.php/search" method="get" class="ccm-search-block-form" role="search">
            <input name="search_paths[]" type="hidden" value="" />
            <input name="query" type="text" placeholder="SEARCH" value="" class="ccm-search-block-text" autocomplete="off"/>
            <button name="submit" class="btn btn-default ccm-search-block-submit">Search</button>
          </form>
        </div>
      </div>
    </div>
  </main>
</div>
<?php $this->inc('elements/footer.php'); ?>

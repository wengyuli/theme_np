<?php
/**
 * Events right sidebar template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$title_img = $view->getThemePath().'/css/images/Banner-Placeholder.jpg';
$banner = $c->getAttribute('header_img');
if ($banner)
  $title_img = $banner->getRelativePath();
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
</style>
<div class="container-fluid interior right-sidebar event" style="margin-bottom: 0px">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
     <div class="text-center">
       <div class="text-title">
         <?php
           $ti = new Area('Page Title');
           $ti->display($c);
         ?>
        </div>
      </div>
    </div>
  </div>
</div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-content">
          <?php
            $a = new Area('Main');
            $a->display($c);
          ?>
        </div>
        <div class="col-md-4 hidden-sm hidden-xs col-sidebar">
	        <div id="affixed-form">
		        <?php
		        $im = new Area('Image');
		        $im->setAreaGridMaximumColumns(12);
		        $im->display($c);
		        ?>
		        <?php
		        $fs = new GlobalArea('Footer Social');
		        $fs->setAreaGridMaximumColumns(12);
		        $fs->display($c);
		        ?>
		        <?php
		        $sf = new GlobalArea('Sidebar Form');
		        if ($sf->getTotalBlocksInArea($c) > 0) echo '<div class="sidebar-form">';
		        $sf->display($c);
		        if ($sf->getTotalBlocksInArea($c) > 0) echo '</div>';
		        ?>
		        <?php
		        $sc = new GlobalArea('Sidebar Content');
		        if ($sc->getTotalBlocksInArea() > 0) echo '<div class="sidebar-cta">';
		        $sc->display($c);
		        if ($sc->getTotalBlocksInArea() > 0) echo '</div>';
		        ?>
		        <?php
		        $sn = new GlobalArea('Sidebar Nav');
		        $sn->setBlockLimit(1);
		        $sn->display($c);
		        ?>

		        <?php
		        $ti = new Area('Tile');
		        $ti->setBlockLimit(1);
		        $ti->display($c);
		        ?>
		        <?php
		        $fa = new Area('Favorites');
		        $fa->setBlockLimit(1);
		        $fa->display($c);
		        ?>
		        <?php
		        $ins = new Area('Instagarm');
		        $ins->setBlockLimit(1);
		        $ins->display($c);
		        ?>

	        </div>
        </div>
      </div>

	    <div class="row popular_posts">
            <div class="row" style="padding-bottom: 15px">
                <div class="prev">
                </div>
                <h3 class="popular_title">POPULAR POSTS</h3>
                <div class="next">
                </div>
            </div>
		    <?php
		    $a = new Area('Popular Posts');
		    $a->setAreaGridMaximumColumns(12);
		    $a->display($c);
		    ?>
	    </div>
    </div> <!-- /container -->
  </main>
 <!-- /container-fluid -->
<?php $this->inc('elements/footer.php'); ?>
<?php if (!$c->isEditMode()): ?>
<script>
  $(window).load(function () {
//    if ($('.right-sidebar').innerHeight() > 1000) {
//      $top = $('.main-nav-header').height() + $('#title-banner').height() + $('.page-sub-header').height();
//      $('#affixed-form').affix({
//        offset: {
//          top: $top,
//          bottom: $('#visual-footer').height() + 50
//        }
//      });
//    }
  });
</script>
<?php endif; ?>

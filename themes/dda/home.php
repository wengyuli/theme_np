<?php
/**
 * Home page template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<section id="video">
	<div class="container-fluid">
		<?php
		$a = new Area('Video');
		$a->setBlockLimit(1);
		$a->display($c);
		?>
	</div>
</section>



<section id="main">
  <div class="container homepage">
    <?php
      $a = new Area('Main');
      $a->enableGridContainer();
      $a->display($c);
    ?>
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3">
        <?php
          $a = new Area('Main - Tiles 1');
          $a->setBlockLimit(1);
          $a->display($c);
        ?>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <?php
          $a = new Area('Main - Tiles 2');
          $a->setBlockLimit(1);
          $a->display($c);
        ?>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <?php
          $a = new Area('Main - Tiles 3');
          $a->setBlockLimit(1);
          $a->display($c);
        ?>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3">
        <?php
          $a = new Area('Main - Tiles 4');
          $a->setBlockLimit(1);
          $a->display($c);
        ?>
      </div>
    </div>
  </div>
</section>

<section id="HomeSpotlight">
<!--	--><?php
//	$a = new Area('Carousel');
//	$a->setBlockLimit(1);
//	$a->display($c);
//	?>
	  <div class="container-fluid <?php if ($c->isEditMode()){ ?>edit-mode<?php } ?>">
	    <?php
	      $sc = new Area('HomeSpotlight');
	      $sc->setBlockLimit(1);
	      # if (!$c->isEditMode()) {
	        $sc->display($c);
	     #  }
	    ?>
	  </div>
<!--	  <div class="mobile-spotlight --><?php //if ($c->isEditMode()){ ?><!--edit-mode--><?php //} ?><!--">-->
<!--	    --><?php //$sc->display($c); ?>
<!--	  </div>-->

</section>

<!--<section id="map">-->
<!--  <div class="container-fluid">-->
    <?php
//      $a = new Area('Map');
//      $a->display($c);
    ?>
<!--  </div>-->
<!--</section>-->

<section id="social">
  <div class="container">
    <?php
      $a = new Area('Social & Events');
      $a->enableGridContainer();
      $a->display($c);
    ?>
  </div>
</section>

<div class="hero">
	<?php
	$a = new Area('Image');
	$a->setBlockLimit(1);
	$a->display($c);
	?>

</div>

<?php if (!$c->isEditMode()) { ?>
<script>
  $(document).ready(function(){
    $('.jump-to-conclusions').on('click',function (e) {
      e.preventDefault();
      var target = this.hash;
      var $target = $(target);
      $('html, body').animate({
        'scrollTop': $target.offset().top
      }, 900, 'swing');
    });
  });
</script>
<?php } ?>
<?php // TODO: This Javascript needs some work and should be in its own file ?>
<script src="<?php echo $this->getThemePath()?>/js/jquery.matchHeight.min.js"></script>
<script>
  $(function(){
    $('.tile').matchHeight();
  });
</script>
<?php $this->inc('elements/footer.php'); ?>

<?php
/**
 * Three column template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$title_img = $view->getThemePath().'/css/images/Banner-Placeholder.jpg';
$banner = $c->getAttribute('header_img');
if ($banner)
  $title_img = $banner->getRelativePath();
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
</style>
<div class="container-fluid interior three-column">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
      <div class="text-center">
        <div class="text-title">
          <?php
            $ti = new Area('Page Title');
            $ti->display($c);
          ?>
        </div>
      </div>
    </div>
  </div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 page-sub-header">
<!--        --><?php
//          $a = new Area('Page Header');
//          $a->display($c);
//        ?>
        </div>
      </div>
      <div class="tiles">
          <?php
            $a = new Area('Content');
            $a->setAreaGridMaximumColumns(12);
            $a->display($c);
          ?>
	      <?php
	      $a = new Area('Main');
	      $a->setAreaGridMaximumColumns(12);
	      $a->display($c);
	      ?>

	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 1');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>
	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 2');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>
	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 3');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>
	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 4');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>
	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 5');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>
	      <div class="col-xs-4">
		      <?php
		      $a = new Area('Main - Tiles 6');
		      $a->setBlockLimit(1);
		      $a->display($c);
		      ?>
	      </div>

        </div>

      </div>
    </div>
  </main>
</div>
<script src="<?php echo $this->getThemePath()?>/js/jquery.matchHeight.min.js"></script>
<script>
  $(function(){
    $('.tile').matchHeight();
  });
</script>
<?php $this->inc('elements/footer.php'); ?>

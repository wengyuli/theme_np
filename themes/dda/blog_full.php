<?php
/**
 * Blog Full (default) template
 **/
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
$title_img = $view->getThemePath().'/css/images/Banner-Placeholder.jpg';
$banner = $c->getAttribute('header_img');
if ($banner)
  $title_img = $banner->getRelativePath();
?>
<style>
  #title-banner { background-image: url('<?php echo $title_img; ?>');}
</style>
<div class="container-fluid interior full ">
  <div class="row page-title" id="title-banner">
    <div class="col-sm-12">
      <div class="text-center">
        <div class="text-title">
          <?php
            $ti = new Area('Page Title');
            $ti->display($c);
          ?>
        </div>
      </div>
    </div>
  </div>
  <main>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 page-sub-header">
        <?php
          $a = new Area('Page Header');
          $a->display($c);
        ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <?php
            $a = new Area('Main');
            $a->setAreaGridMaximumColumns(12);
            $a->display($c);
          ?>
        </div>
      </div>
    </div>
  </main>
</div> <!-- /container-fluid -->

<?php $this->inc('elements/footer.php'); ?>

<?php
namespace Concrete\Package\ThemeNp\Theme\Dda;
use Concrete\Core\Page\Theme\Theme;

class PageTheme extends Theme
{
  protected $pThemeGridFrameworkHandle = 'bootstrap3'; 

  public function registerAssets()
  {
    $this->providesAsset('css', 'bootstrap/*');
    $this->requireAsset('css', 'font-awesome');
    $this->providesAsset('javascript', 'bootstrap/*');
    $this->requireAsset('javascript', 'jquery');
    $this->requireAsset('javascript', 'spectrum');
    $this->requireAsset('javascript', 'underscore');
  }

  public function getThemeEditorClasses()
  {
    return array(
      array('title' => t('Primary Button'), 'menuClass' => '', 'spanClass' => 'btn btn-primary'),
      array('title' => t('Secondary Button'), 'menuClass' => '', 'spanClass' => 'btn btn-secondary'),
    );
  }
}
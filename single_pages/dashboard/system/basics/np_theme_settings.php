<?php defined('C5_EXECUTE') or die("Access Denied.");
  $fp = FilePermissions::getGlobal();
  $tp = new TaskPermission();
  $al = Loader::helper('concrete/asset_library');
  $bf = null;
  if ($this->controller->getFileID() > 0) {
    $bf = $this->controller->getFileObject();
  }
  $args = array();
?>

<form action="<?php echo View::action('update_theme_settings') ?>" method="post">
  <?php echo $this->controller->token->output('update_theme_settings')?>
  <fieldset>
  <legend><?php echo t('Instagram API') ?></legend>
    <div class="form-group">
      <label for="INSTAGRAM-ACCESS-TOKEN" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Instagram Access Token is obtained from Instagram developer tools. I identifies the application.')?>"><?php echo t('Instagram Access Token')?></label>
      <?php echo $form->text('INSTAGRAM-ACCESS-TOKEN', $ig_access_token, array('class' => 'span4'))?>
    </div>
    <div class="form-group">
      <label for="INSTAGRAM-USER-ID" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Instagram USER ID is obtained from Instagram developer tools. I identifies the application.')?>"><?php echo t('Instagram USER ID')?></label>
      <?php echo $form->text('INSTAGRAM-USER-ID', $ig_user_id, array('class' => 'span4'))?>
    </div>
  </fieldset>
  <fieldset>
    <legend><?php echo t('Google API') ?></legend>
    <div class="form-group">
      <label for="GOOGLE-API-KEY" class="launch-tooltip control-label" data-placement="right" title="<?php echo t('Unique key for accessing Google API.')?>"><?php echo t('Key')?></label>
      <?php echo $form->text('GOOGLE-API-KEY', $google_api_key, array('class' => 'span4'))?>
    </div>
  </fieldset>
  <fieldset>
    <legend>Merchant Directory</legend>
    <div class="form-group">
      <label>Search Page</label>
      <?php
        $pgp = Loader::helper('form/page_selector');
        echo $pgp->selectPage('merchant-search', $merchant_search);
      ?>
    </div>
    <div class="form-group">
      <label>Merchant Page Default Header Image</label>
      <?php echo $al->image('ccm-b-image', 'merchant-header-img', t('Choose Image'), $bf, $args);?>
    </div>
  </fieldset>
  <div class="ccm-dashboard-form-actions-wrapper">
    <div class="ccm-dashboard-form-actions">
      <button class="pull-right btn btn-primary">
        <?php echo t('Save')?>
      </button>
    </div>
  </div>
</form>

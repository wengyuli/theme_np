<?php  defined('C5_EXECUTE') or die(_("Access Denied."));
$html = Loader::helper('html');
Loader::model('attribute/categories/collection');
$vimeoColor= ltrim ($vvColor, '#');
$c = Page::getCurrentPage();
if (is_object($c) && $c->isEditMode()) { ?>
  <div class="ccm-edit-mode-disabled-item" style="width: <?php  echo $vvWidth ?>px height: <?php  echo $vvHeight ?>px; margin: 0 auto;">
    <div style="padding:8px 0px; padding-top: <?php echo round($vHeight/2)-10; ?>px;"><?php echo t('Vimeo Video disabled in edit mode.'); ?></div>
  </div>
<?php } else { 
  $ratio = isset($aspectRatio) ? $aspectRatio : '16by9';
?>
<div class="embed-responsive embed-responsive-<?php echo $ratio ?>">
	<iframe src="//player.vimeo.com/video/<?php  echo $vimeoVid ?>?autoplay=<?php  echo $autoplay ?>&loop=<?php  echo $vvloop ?>&title=<?php  echo $introTitle ?>&byline=<?php  echo $byline ?>&portrait=<?php  echo $portrait ?>&color=<?php  echo $vimeoColor ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	<?php  if ($showlink == 1) { ?><p><a href="http://vimeo.com/<?php  echo $vimeoVid ?>" target="blank"><?php  echo $vvTitle ?></a><?php  echo t(' from ')?><a href="http://vimeo.com/<?php  echo $vvUser ?>" target="_blank"><?php  echo $vvUser ?></a><?php  echo t(' on ')?><a href="https://vimeo.com" target="_blank"><?php  echo t('Vimeo')?></a>.</p><?php  } ?>
</div>
<?php  } ?>
<?php  defined('C5_EXECUTE') or die(_("Access Denied."));
$color = Core::make('helper/form/color');
$addSelected = true;
?>

<div class="ccm-block-field-group">
	<div class="row">
		<fieldset>
			<div class="col-xs-6">
				<?php   echo $form->label('vimeoVid', t('<a href="http://www.vimeo.com" target="_blank">Vimeo</a> Video ID:'));?>
				<div class="input-group">
					<?php   echo $form->text('vimeoVid', $vimeoVid);?>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<label class="form-label"><?php  echo t('Color')?></label><br />
					<?php  $color->output('vvColor', $vvColor?$vvColor:"#00adef", array('preferredFormat'=>'hex'));?>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="ccm-block-field-group">
	<div class="row">
		<fieldset>
			<div style="font-style: italic">For fixed width view</div>
			<div class="col-xs-6">
				<div class="form-group">
					<?php   echo $form->label('vvWidth', t('Width:'));?>
					<div class="input-group">
						<?php   echo $form->text("vvWidth",$vvWidth?$vvWidth:"500"); ?> 
						<div class="input-group-addon"><?php  echo t('px')?></div> 
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<?php   echo $form->label('vvHeight', t('Height:'));?>
					<div class="input-group">
						<?php   echo $form->text("vvHeight",$vvHeight?$vvHeight:"280"); ?> 
						<div class="input-group-addon"><?php  echo t('px')?></div> 
					</div>
				</div>
			</div>
    </fieldset>
	</div>
</div>
<div class="ccm-block-field-group">
	<div class="row">
		<fieldset>
			<div style="font-style: italic">For responsize (default) view</div>
			<div class="col-xs-6">
				<div class="form-group">
					<?php   echo $form->label('aspectRatio', t('Aspect Ratio:'));?>
					<div class="input-group">
						 <select name="aspectRatio">
				      <option value="16by9" <?php if ($aspectRatio == '16by9') echo 'selected="selected"'; ?>>16:9</option>
				      <option value="4by3" <?php if ($aspectRatio == '4by3') echo 'selected="selected"'; ?>>4:3</option>
				    </select>
					</div>
				</div>
			</div>
    </fieldset>
	</div>
</div>
<div class="ccm-block-field-group">
	<div class="row">
		<fieldset>
			<div class="col-xs-6">
				<?php   echo $form->label('vvTitle', t('Video Title:'));?>
				<div class="input-group">
					<?php   echo $form->text('vvTitle', $vvTitle);?>
				</div>
			</div>
			<div class="col-xs-6">
				<?php   echo $form->label('vvUser', t('Vimeo Username:'));?>
				<div class="input-group">
					<div class="input-group-addon">@</div>
					<?php   echo $form->text('vvUser', $vvUser);?>
				</div>
			</div>
		</fieldset>
	</div>
</div>
<div class="ccm-block-field-group">
	<div class="row">
		<fieldset>
			<div class="col-xs-6">
				<h4><?php  echo t('Intro: ')?></h4>
				<p><input type="checkbox" name="introTitle" value="1" <?php  if ($introTitle == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Title');?></p>
				<p><input type="checkbox" name="portrait" value="1" <?php  if ($portrait == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Portrait');?></p>
				<p><input type="checkbox" name="byline" value="1" <?php  if ($byline == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Byline');?></p>
			</div>
			<div class="col-xs-6">
				<h4><?php  echo t('Other: ')?></h4>
				<p><input type="checkbox" name="autoplay" value="1" <?php  if ($autoplay == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Autoplay video');?></p>
				<p><input type="checkbox" name="vvloop" value="1" <?php  if ($vvloop == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Loop video');?></p>
				<p><input type="checkbox" name="showlink" value="1" <?php  if ($showlink == 1) { ?> checked <?php  } ?>  /> <?php  echo t('Display link under video');?></p>
			</div>
		</fieldset>
	</div>
</div>
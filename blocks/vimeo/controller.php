<?php  
namespace Concrete\Package\ThemeNp\Block\Vimeo;

use \Concrete\Core\Block\BlockController,
BlockType,
Loader;
defined('C5_EXECUTE') or die(_("Access Denied."));

	class Controller extends BlockController {
		
		var $pobj;
		
		protected $btTable = 'btVimeoVid';
		protected $btInterfaceWidth = "430";
		protected $btInterfaceHeight = "460";
		protected $btDefaultSet = 'multimedia';
		
		public function getBlockTypeDescription() {
			return t("Add a Vimeo video to your page.");
		}
		public function getBlockTypeName() {
			return t("Vimeo Video");
		}

		function save($data) { 
			$db = Loader::db();
			//Settings Data
			$args['vimeoVid'] 	= $data['vimeoVid'];
			$args['vvTitle'] 	= $data['vvTitle'];
			$args['vvUser'] 	= $data['vvUser'];
			$args['vvHeight'] 	= is_numeric($data['vvHeight']) ? intval($data['vvHeight']) : '280';
			$args['vvWidth'] 	= is_numeric($data['vvWidth']) ? intval($data['vvWidth']) : '500';
			$args['vvColor'] = isset($data['vvColor']) ? $data['vvColor'] : '#00adef';
			
			$args['autoplay'] = ($data['autoplay']) ? '1' : '0';
			$args['vvloop'] = ($data['vvloop']) ? '1' : '0';
			$args['showlink'] = ($data['showlink']) ? '1' : '0';
			
			$args['introTitle'] = ($data['introTitle']) ? '1' : '0';
			$args['portrait'] = ($data['portrait']) ? '1' : '0';
			$args['byline'] = ($data['byline']) ? '1' : '0';

			$args['aspectRatio'] = ($data['aspectRatio']) ? $data['aspectRatio'] : '16by9';
		
			parent::save($args);
		}
		
	}
?>
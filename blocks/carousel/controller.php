<?php
namespace Concrete\Package\ThemeNp\Block\Carousel;

use \Concrete\Core\Block\BlockController;
use Loader;
use Page;

class Controller extends BlockController {

  protected $btTable = 'btCarousel';
  protected $btExportTables = array('btCarousel', 'btCarouselSlides');
  protected $btDefaultSet = "multimedia";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btExportFileColumns = array('fID');
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;

  public function getBlockTypeDescription() {
    return t("Carousel block with call-to-action buttons");
  }

  public function getBlockTypeName() {
    return t("Carousel");
  }

  public function getSearchableContent() {
    $content = '';
    $db = Loader::db();
    $v = array($this->bID);
    $q = 'select * from btCarouselSlides where bID = ?';
    $r = $db->query($q, $v);
    foreach ($r as $row) {
        $content .= $row['heading'] . ' ' . $row['subHeading'];
    }
    return $content;
  }

  public function add() {
    $this->requireAsset('core/file-manager');
    $this->requireAsset('core/sitemap');
    $this->requireAsset('redactor');
  }

  public function edit() {
    $this->add();
    $db = Loader::db();
    $query = $db->GetAll('SELECT * from btCarouselSlides WHERE bID = ? ORDER BY sortOrder', array($this->bID));
    $this->set('rows', $query);
  }

  public function view() {
    $db = Loader::db();
    $query = $db->GetAll('SELECT * from btCarouselSlides WHERE bID = ? ORDER BY sortOrder', array($this->bID));
    $this->set('rows', $query);
  }

  public function duplicate($newBID) {
    $db = Loader::db();
    $v = array($this->bID);
    $q = 'select * from btCarouselSlides where bID = ?';
    $r = $db->query($q, $v);
    foreach ($r as $row) {
      $db->execute(
        'INSERT INTO btCarouselSlides (bID, fID, heading, subHeading, cta, linkedPageCID, externalURL, sortOrder) values(?,?,?,?,?,?,?,?)',
        array(
          $newBID,
          $row['fID'],
          htmlentities($row['heading'], ENT_QUOTES),
          htmlentities($row['subHeading'], ENT_QUOTES),
          htmlentities($row['cta'], ENT_QUOTES),
          $row['linkedPageCID'],
          $row['externalURL'],
          $row['sortOrder']
        )
      );
    }
  }

  public function delete() {
    $db = Loader::db();
    $db->execute('DELETE from btCarouselSlides WHERE bID = ?', array($this->bID));
    parent::delete();
  }

  public function save($args) {
    $db = Loader::db();
    $db->execute('DELETE from btCarouselSlides WHERE bID = ?', array($this->bID));
    $count = count($args['sortOrder']);
    $i = 0;
    if ($args['maxHeight'] == '')
      $args['maxHeight'] = null;
    parent::save($args);
    while ($i < $count) {
      $db->execute(
        'INSERT INTO btCarouselSlides (bID, fID, heading, subHeading, cta, linkedPageCID, externalURL, sortOrder) values(?,?,?,?,?,?,?,?)',
        array(
          $this->bID,
          intval($args['fID'][$i]),
          htmlentities($args['heading'][$i], ENT_QUOTES),
          htmlentities($args['subHeading'][$i], ENT_QUOTES),
          htmlentities($args['cta'][$i], ENT_QUOTES),
          intval($args['linkedPageCID'][$i]),
          $args['externalURL'][$i],
          $args['sortOrder'][$i]
        )
      );
      $i++;
    }
  }
}
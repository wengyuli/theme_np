<?php  defined('C5_EXECUTE') or die("Access Denied.");

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();
$pageSelector = Loader::helper('form/page_selector');
?>
<script>
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Loader::helper('validation/token')->generate('editor')?>";
    $(document).ready(function(){
        var ccmReceivingEntry = '';
        var sliderEntriesContainer = $('.ccm-cta-slider-entries');
        var _templateSlide = _.template($('#imageTemplate').html());
        var attachDelete = function($obj) {
            $obj.click(function(){
                var deleteIt = confirm('<?php echo t('Are you sure?') ?>');
                if(deleteIt == true) {
                    $(this).closest('.ccm-cta-slider-entry').remove();
                    doSortCount();
                }
            });
        }

        var attachSortDesc = function($obj) {
            $obj.click(function(){
               var myContainer = $(this).closest($('.ccm-cta-slider-entry'));
               myContainer.insertAfter(myContainer.next('.ccm-cta-slider-entry'));
               doSortCount();
            });
        }

        var attachSortAsc = function($obj) {
            $obj.click(function(){
                var myContainer = $(this).closest($('.ccm-cta-slider-entry'));
                myContainer.insertBefore(myContainer.prev('.ccm-cta-slider-entry'));
                doSortCount();
            });
        }

        var attachFileManagerLaunch = function($obj) {
            $obj.click(function(){
                var oldLauncher = $(this);
                ConcreteFileManager.launchDialog(function (data) {
                    ConcreteFileManager.getFileDetails(data.fID, function(r) {
                        jQuery.fn.dialog.hideLoader();
                        var file = r.files[0];
                        oldLauncher.html(file.resultsThumbnailImg);
                        oldLauncher.next('.image-fID').val(file.fID)
                    });
                });
            });
        }

        var doSortCount = function(){
            $('.ccm-cta-slider-entry').each(function(index) {
                $(this).find('.ccm-cta-slider-entry-sort').val(index);
            });
        };

        var makeID = function() {
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          for( var i=0; i < 10; i++ )
              text += possible.charAt(Math.floor(Math.random() * possible.length));
          return text;
        }



       <?php if($rows) {
           foreach ($rows as $row) { ?>
           sliderEntriesContainer.append(_templateSlide({
                fID: '<?php echo $row['fID'] ?>',
                <?php if(File::getByID($row['fID'])) { ?>
                image_url: '<?php echo File::getByID($row['fID'])->getThumbnailURL('file_manager_listing');?>',
                <?php } else { ?>
                image_url: '',
               <?php } ?>
                heading: "<?php echo $row['heading'] ?>",
                subHeading: '<?php echo $row['subHeading'] ?>',
                cta: '<?php echo $row['cta'] ?>',
                <?php if($row['linkedPageCID']){
                  $page = Page::getByID($row['linkedPageCID']);
                  $pageName = $page->getCollectionName();
                }
                ?>
                linkedPageCID: '<?php echo $row['linkedPageCID'] ?>',
                externalURL: '<?php echo $row['externalURL'] ?>',
                pageName: '<?=$pageName?>',
                sort_order: '<?php echo $row['sortOrder'] ?>',
            }));

        <?php }
        }?>

        doSortCount();

        $('.ccm-add-cta-slider-entry').click(function(){
           var thisModal = $(this).closest('.ui-dialog-content');
            sliderEntriesContainer.append(_templateSlide({
                fID: '',
                heading: '',
                subHeading: '',
                cta: '',
                linkedPageCID: '',
                externalURL: '',
                pageName: '',
                sort_order: '',
                image_url: ''
            }));

            var newSlide = $('.ccm-cta-slider-entry').last();
            thisModal.scrollTop(newSlide.offset().top);
            attachDelete(newSlide.find('.ccm-delete-cta-slider-entry'));
            attachFileManagerLaunch(newSlide.find('.ccm-pick-slide-image'));
            newSlide.find('div[data-field=entry-link-page-selector-select]').concretePageSelector({
                'inputName': 'internalLinkCID[]'
            });
            attachSortDesc(newSlide.find('i.fa-sort-desc'));
            attachSortAsc(newSlide.find('i.fa-sort-asc'));
            doSortCount();
        });
        attachDelete($('.ccm-delete-cta-slider-entry'));
        attachSortAsc($('i.fa-sort-asc'));
        attachSortDesc($('i.fa-sort-desc'));
        attachFileManagerLaunch($('.ccm-pick-slide-image'));
    });
</script>
<style>

    .ccm-cta-slider-block-container .redactor_editor {
        padding: 20px;
    }
    .ccm-cta-slider-block-container input[type="text"],
    .ccm-cta-slider-block-container textarea {
        display: block;
        width: 100%;
    }
    .ccm-cta-slider-block-container .btn-success {
        margin-bottom: 20px;
    }

    .ccm-cta-slider-entries {
        padding-bottom: 30px;
    }

    .ccm-pick-slide-image {
        padding: 15px;
        cursor: pointer;
        background: #dedede;
        border: 1px solid #cdcdcd;
        text-align: center;
        vertical-align: center;
    }

    .ccm-pick-slide-image img {
        max-width: 100%;
    }

    .ccm-cta-slider-entry {
        position: relative;
    }



    .ccm-cta-slider-block-container i.fa-sort-asc {
        position: absolute;
        top: 10px;
        right: 10px;
        cursor: pointer;
    }

    .ccm-cta-slider-block-container i:hover {
        color: #5cb85c;
    }

    .ccm-cta-slider-block-container i.fa-sort-desc {
        position: absolute;
        top: 15px;
        cursor: pointer;
        right: 10px;
    }
    .ccm-cta-slider-block-container .help {
        font-size: 80%;
        font-style: italic;
    }
</style>
<div class="ccm-cta-slider-block-container">
  <fieldset>
    <legend>Settings</legend>
    <div class="form-group">
      <label><?php echo t('Tag Line') ?></label>
      <?php print $form->text('tagLine', $tagLine); ?>
      <div class="help">
        Optional. The tag line is text that will appear below the carousel.
      </div>
    </div>
    <div class="form-group">
      <label><?php echo t('Speed') ?></label>
      <select name="speed">
        <option value="3500"<?php if ($speed == 3500) echo ' selected' ?>>Fast</option>
        <option value="5000"<?php if ($speed == 5000 || is_null($speed)) echo ' selected' ?>>Medium</option>
        <option value="8500"<?php if ($speed == 8500) echo ' selected' ?>>Slow</option>
        <option value="false"<?php if ($speed == 'false') echo ' selected' ?>>Don't Cycle</option>
      </select>
    </div>
    <div class="form-group">
      <label><?php echo t('Max Height') ?></label>
      <?php print $form->number('maxHeight', $maxHeight); ?>
      <div class="help">Leave blank for 100% height</div>
    </div>
  </fieldset>
  <fieldset>
    <legend>Slides</legend>
    <span class="btn btn-success ccm-add-cta-slider-entry"><?php echo t('Add Entry') ?></span>
    <div class="ccm-cta-slider-entries">

    </div>
  </fieldset>
</div>
<script type="text/template" id="imageTemplate">
    <div class="ccm-cta-slider-entry well">
        <i class="fa fa-sort-desc"></i>
        <i class="fa fa-sort-asc"></i>
        <div class="form-group">
            <label><?php echo t('Image') ?></label>
            <div class="ccm-pick-slide-image">
                <% if (image_url.length > 0) { %>
                    <img src="<%= image_url %>" />
                <% } else { %>
                    <i class="fa fa-picture-o"></i>
                <% } %>
            </div>
            <input type="hidden" name="<?php echo $view->field('fID')?>[]" class="image-fID" value="<%=fID%>" />
        </div>
        <div class="form-group">
            <label><?php echo t('Heading') ?></label>
            <input type="text" name="<?php echo $view->field('heading')?>[]" value="<%=heading%>" />
        </div>
        <div class="form-group">
            <label><?php echo t('Sub Heading') ?></label>
            <input type="text" name="<?php echo $view->field('subHeading')?>[]" value="<%=subHeading%>" />
        </div>
        <div class="form-group">
            <label><?php echo t('Call to Action') ?></label>
            <input type="text" name="<?php echo $view->field('cta')?>[]" value="<%=cta%>" />
        </div>
        <!-- PAGE SELECTOR --->
        <div class="form-group">
          <label><?php echo t('Link to an External URL') ?></label>
          <input type="text" name="<?php echo $view->field('externalURL')?>[]" value="<%=externalURL%>" />
          <div class="help">External URL takes precendent, leave empty to change to internal link.</div>
        </div>
        <div class="form-group">
          <label><?=t('Link to an Internal Page')?></label>
          <div id="select-page-<%=sort_order%>">
            <?php $this->inc('elements/page_selector.php');?>
          </div>
        </div>
        <input class="ccm-cta-slider-entry-sort" type="hidden" name="<?php echo $view->field('sortOrder')?>[]" value="<%=sort_order%>"/>
        <div class="form-group">
            <span class="btn btn-danger ccm-delete-cta-slider-entry"><?php echo t('Delete Entry'); ?></span>
        </div>
    </div>
</script>

<?php defined('C5_EXECUTE') or die('Access Denied.');
$th = Loader::helper('text');
if (isset($error)) {
    ?><?php echo $error?><br/><br/><?php
}
if (!isset($query) || !is_string($query)) {
    $query = '';
} ?>
<form action="<?php echo $view->url($resultTargetURL)?>" method="get" class="ccm-search-block-form" id="merch-search-form"><?php
  if (isset($title) && ($title !== '')) { ?>
    <h3><?php echo h($title)?></h3>
<?php }
  if ($query === '') { ?>
    <input name="search_paths[]" type="hidden" value="<?php echo htmlentities($baseSearchPath, ENT_COMPAT, APP_CHARSET) ?>" />
<?php } elseif (isset($_REQUEST['search_paths']) && is_array($_REQUEST['search_paths'])) {
    foreach ($_REQUEST['search_paths'] as $search_path) { ?>
      <input name="search_paths[]" type="hidden" value="<?php echo htmlentities($search_path, ENT_COMPAT, APP_CHARSET) ?>" /><?php
    }
  } ?>
  <input name="query" type="text" value="<?php echo htmlentities($query, ENT_COMPAT, APP_CHARSET)?>" class="ccm-search-block-text" id="query" />
<?php if (isset($buttonText) && ($buttonText !== '')) { ?>
  <input name="submit" type="submit" value="<?php echo h($buttonText)?>" class="btn btn-default ccm-search-block-submit" />
</form>
<?php }
    if (isset($do_search) && $do_search) {
      if (count($results) == 0) { ?>
      <h4 style="margin-top:32px"><?php echo t('There were no results found. Please try another keyword or phrase.')?></h4><?php
      } else {
        $tt = Core::make('helper/text'); ?>
        <div class="tile-page-list merchant row">
        <?php
          foreach ($results as $page) {
            $title = $th->entities($page->getCollectionName());
            $url = $page->getCollectionLink();
            $thumbnail = $page->getAttribute('merchant_feature_image');
            $description = $page->getCollectionDescription();
            $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
            $description = $th->entities($description);
            // @TODO: Fetch all other attributes
            $controller = $page->getPageController();
        ?>
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="tile-container white">
              <div class="tile absolute-button">
              <?php 
              $image = '';
              if ($page->getAttribute('merchant_dtca_member') && is_object($thumbnail)) {
                $imgHelper = Loader::helper('image');
                $image = $imgHelper->getThumbnail($thumbnail, 500, 500)->src; 
              }
              else {
                $merchant_cat_class = preg_replace('~[^\\pL\d]+~u', '-', html_entity_decode($page->getAttribute('merchant_category')));
                $image = $view->getThemePath() . '/css/images/shopping_icons/' . strtolower($merchant_cat_class) . '.png';
              }
              //elseif ($controller->get_places_img_src($page)) {
              //  $image = $controller->get_places_img_src($page);
              //}
              echo '<div class="merchant-image" style="background-image: url(\''.$image.'\'); background-position: center;"></div>';
              ?>
                <div>
                  <h4><?php echo $title; ?></h4>
                  <p>
                  <?php if ($page->getAttribute('merchant_dtca_member')) {
                    echo $description;
                  } ?>
                  </p>
                </div>
                <div>
                  <ul class="merchant-details">
                  <?php
                    if ($address = $controller->get_address($page))
                      echo sprintf("<li class=\"address\"><span>%s</span></li>", $address);
                    if ($phone = $controller->get_phone($page))
                      echo sprintf("<li class=\"phone\"><span>%s</span></li>", $phone);
                    if ($page->getAttribute('merchant_np_member') && $controller->get_website($page))
                      echo sprintf("<li class=\"website\"><span>%s</span></li>", $controller->get_website($page, true)); 
                  ?>
                  </ul>
                </div>  
                <div>
                  <a href="<?php echo $url?>" class="btn btn-secondary"><?php echo isset($buttonText) ? $buttonText : 'View' ?></a>
                </div>
              </div>
            </div>
          </div> <!-- end .col-* -->
          <?php } ?>
          </div>
          <?php
            $pages = $pagination->getCurrentPageResults();
            if ($pagination->getTotalPages() > 1 && $pagination->haveToPaginate()) {
                $showPagination = true;
                echo $pagination->renderDefaultView();
            }
        }
    }
    else if (count($categories) > 0) { ?>
      <ul class="list-group merchant-search-categories">
        <?php //div class="row"> ?>
        <?php foreach ($categories as $cat) { 
          $class = preg_replace('~[^\\pL\d]+~u', '-', $cat);
        ?>
          <li class="list-group-item col-xs-12 col-sm-6 col-md-4">
            <a href="<?php echo $view->url($resultTargetURL)?>?category=<?php echo urlencode($cat) ?>" class="h5 shopping shopping--<?php echo strtolower($class); ?>"><?php echo $cat ?></a>
          </li>
        <?php } ?>
      </ul>
<?php }
?>





<?php

namespace Concrete\Package\ThemeNp\Block\MerchantSearch;

use \Concrete\Block\Search\Controller as SearchBlockController;
use \Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;
use Concrete\Core\Page\PageList;
use Page;
use Core;

class Controller extends SearchBlockController {

  protected $btDefaultSet = "form";

  public function getBlockTypeDescription(){
    return t("Add a merchant category list and search box to your site.");
  }

  public function getBlockTypeName(){
    return t("Merchant Search");
  }

  public function view() {  
    $c = Page::getCurrentPage();
    $this->set('title', $this->title);
    $this->set('buttonText', $this->buttonText);
    $this->set('baseSearchPath', $this->baseSearchPath);
    $this->set('postTo_cID', $this->postTo_cID);

    $resultsURL = $c->getCollectionPath();

    if ($this->resultsURL != '') {
      $resultsURL = $this->resultsURL;
    } elseif ($this->postTo_cID != '') {
      $resultsPage = Page::getById($this->postTo_cID);
      $resultsURL = $resultsPage->cPath;
    }

    $resultsURL = Core::make('helper/text')->encodePath($resultsURL);

    $this->set('resultTargetURL', $resultsURL);

    //run query if display results elsewhere not set, or the cID of this page is set
    if ($this->postTo_cID == '' && $this->resultsURL == '') {
      if (!empty($_REQUEST['query']) || isset($_REQUEST['akID']) || isset($_REQUEST['month'])) {
        $this->do_search();
      }
      else if (!empty($_REQUEST['category'])) {
        $this->do_search_by_category();
      }
      else {
        $this->get_categories();
      }
    }
  }

  // TODO: Override the search method to look for category parameter and search by param instead for string
  public function do_search_by_category() {
    $q = $_REQUEST['category'];
    // i have NO idea why we added this in rev 2000. I think I was being stupid. - andrew
    // $_q = trim(preg_replace('/[^A-Za-z0-9\s\']/i', ' ', $_REQUEST['query']));
    $_q = $q;

    $ipl = new PageList();
    $aksearch = false;

    if (isset($_REQUEST['category'])) {

      $ipl->filterByAttribute('merchant_category', $_REQUEST['category']);
    }

    if (is_array($_REQUEST['search_paths'])) {
      foreach ($_REQUEST['search_paths'] as $path) {
        if (!strlen($path)) {
          continue;
        }
        $ipl->filterByPath($path);
      }
    } elseif ($this->baseSearchPath != '') {
      $ipl->filterByPath($this->baseSearchPath);
    }
    $ipl->sortByName();

    $pagination = $ipl->getPagination();
    $pagination->setMaxPerPage(12);
    $results = $pagination->getCurrentPageResults();

    $this->set('query', $q);
    $this->set('results', $results);
    $this->set('do_search', true);
    $this->set('searchList', $ipl);
    $this->set('pagination', $pagination);
  }

  private function get_categories() {
    $catAttr = CollectionAttributeKey::getByHandle('merchant_category');
    $categories = array();
    if (is_object($catAttr)) {
      $akc = $catAttr->getController();
      $arrOptions = $akc->getOptions();
      foreach ($arrOptions as $option) {
        $categories[] = $option->value;
      }
    }
    $this->set('categories', $categories);
  }

  public function do_search() {
    $q = $_REQUEST['query'];
    // i have NO idea why we added this in rev 2000. I think I was being stupid. - andrew
    // $_q = trim(preg_replace('/[^A-Za-z0-9\s\']/i', ' ', $_REQUEST['query']));
    $_q = $q;

    $ipl = new PageList();
    $aksearch = false;
    if (is_array($_REQUEST['akID'])) {
        foreach ($_REQUEST['akID'] as $akID => $req) {
            $fak = CollectionAttributeKey::getByID($akID);
            if (is_object($fak)) {
                $type = $fak->getAttributeType();
                $cnt = $type->getController();
                $cnt->setAttributeKey($fak);
                $cnt->searchForm($ipl);
                $aksearch = true;
            }
        }
    }

    if (isset($_REQUEST['month']) && isset($_REQUEST['year'])) {
        $year = @intval($_REQUEST['year']);
        $month = abs(@intval($_REQUEST['month']));
        if (strlen(abs($year)) < 4) {
            $year = (($year < 0) ? '-' : '') . str_pad($year, 4, '0', STR_PAD_LEFT);
        }
        if ($month < 12) {
            $month = str_pad($month, 2, '0', STR_PAD_LEFT);
        }
        $daysInMonth = date('t', strtotime("$year-$month-01"));
        $dh = Core::make('helper/date');
        /* @var $dh \Concrete\Core\Localization\Service\Date */
        $start = $dh->toDB("$year-$month-01 00:00:00", 'user');
        $end = $dh->toDB("$year-$month-$daysInMonth 23:59:59", 'user');
        $ipl->filterByPublicDate($start, '>=');
        $ipl->filterByPublicDate($end, '<=');
        $aksearch = true;
    }

    if (empty($_REQUEST['query']) && $aksearch == false) {
        return false;
    }

    if (isset($_REQUEST['query'])) {
        $ipl->filterByKeywords($_q);
    }

    if (is_array($_REQUEST['search_paths'])) {
        foreach ($_REQUEST['search_paths'] as $path) {
            if (!strlen($path)) {
                continue;
            }
            $ipl->filterByPath($path);
        }
    } elseif ($this->baseSearchPath != '') {
        $ipl->filterByPath($this->baseSearchPath);
    }
    $ipl->sortByName();
    
    // TODO fix this
    //$ipl->filter(false, '(ak_exclude_search_index = 0 or ak_exclude_search_index is null)');

    $pagination = $ipl->getPagination();
    $pagination->setMaxPerPage(12);
    $results = $pagination->getCurrentPageResults();

    $this->set('query', $q);
    $this->set('results', $results);
    $this->set('do_search', true);
    $this->set('searchList', $ipl);
    $this->set('pagination', $pagination);
}

}
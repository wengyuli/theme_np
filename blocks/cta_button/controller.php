<?php
namespace Concrete\Package\ThemeNp\Block\CtaButton;

use \Concrete\Core\Block\BlockController;
use Loader;
use Page;

class Controller extends BlockController {

  protected $btTable = 'btCtaButton';
  protected $btDefaultSet = "basic";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btExportFileColumns = array('fID');
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;

  public function getBlockTypeName() {
    return t('CTA Button');
  }

  public function getBlockTypeDescription () {
    return t('Simple block to display a link as a call-to-action button');
  }

  public function add() {
    $this->requireAsset('core/sitemap');
  }

  public function edit() {
    $this->add();
  }

  public function save($args) {
    if ($args['linkType'] == 1) {
      $args['externalLink'] = '';
    }
    else if ($args['linkType'] == 2) {
      $args['internalLinkCID'] = 0;
    }
    parent::save($args);
  }

}
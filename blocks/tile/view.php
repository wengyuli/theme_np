<?php
defined('C5_EXECUTE') or die("Access Denied.");
$imgHelper = Loader::helper('image');
?>

<div class="tile-container <?php echo $background ?>">
  <div class="tile<?php if (Page::getById($internalLinkCID)->getCollectionPath() && $ctaText) echo ' absolute-button'; ?>">
    <?php
      if ($fID != null) {
        $image = File::getByID($fID);
        if (is_object($image)) {
          $path = $imgHelper->getThumbnail($image, 500, 500)->src;
          echo '<img src="'.$path.'" class="img-responsive" />';
        }
      } ?>
      <div class="tile-content">
        <?php echo $content ?>
      </div>
      <?php if (Page::getById($internalLinkCID)->getCollectionPath() && $ctaText): ?>
        <div>
          <a href="<?php echo Page::getById($internalLinkCID)->getCollectionPath() ?>" class="btn btn-secondary"><?php echo $ctaText ?></a>
        </div>
      <?php endif; ?>
  </div>
</div>

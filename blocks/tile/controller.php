<?php
namespace Concrete\Package\ThemeNp\Block\Tile;

use \Concrete\Core\Block\BlockController;
use Loader;
use \File;
use Page;

class Controller extends BlockController {

  protected $btTable = 'btTile';
  protected $btDefaultSet = "basic";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btExportFileColumns = array('fID');
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;

  public function getBlockTypeDescription() {
    return t("A tile block for use on tiled pages!");
  }

  public function getBlockTypeName() {
    return t("Tile");
  }

  public function getSearchableContent() {
    return $this->content . ' ' . $this->ctaText;
  }

  public function add() {
    $this->requireAsset('core/sitemap');
    $this->requireAsset('core/file-manager');
    $this->requireAsset('redactor');
  }

  public function edit() {
    $this->add();
  }

  public function save($args) {
    $args['fID'] = ($args['fID'] != '') ? $args['fID'] : 0;
    parent::save($args);
  }

  function getFileID() {return $this->fID;}

  public function getFileObject() {
    return File::getByID($this->fID);
  }

}
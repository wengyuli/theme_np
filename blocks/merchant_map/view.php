<?php defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage(); 
if ($c->getCollectionTypeHandle() == 'merchant_page' && $c->getAttribute('merchant_coordinates')) {
  $center = explode(",", $c->getAttribute('merchant_coordinates'));
  $latitude = $center[0];
  $longitude = $center[1];
}
?>
<div class="merchant-map">
<?php if ($c->isEditMode()) { ?>
  <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
    <div style="padding: 80px 0px 0px 0px"><?php echo t('Google Map disabled in edit mode.')?></div>
  </div>
<?php  } else { ?>
  <?php  if( strlen($title)>0){ ?><h3><?php echo $title?></h3><?php  } ?>
  <div id="googleMapCanvas<?php echo $unique_identifier?>" class="googleMapCanvas" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>"></div>
<?php  } ?>
  <div class="container-fluid">
    <div class="row map-filters">
      <div class="col-sm-12 col-md-10">
        <ul class="list-inline">
<?php
  $first = true;
  foreach ($map_cateogries as $cat) { 
    $cat = preg_replace("/\"/", "", $cat); ?>
          <li>
            <h5 class="<?php echo strtolower($cat) ?>">
              <span>
                <?php echo $cat ?>
                <input 
                  type="checkbox" 
                  class="map-filter" 
                  name="map-filter" 
                  value="<?php echo $cat ?>" 
                  <?php if ($cat == 'Parking') { ?>
                  onClick="toggleParking()"
                  <?php } elseif (strtolower($cat) == 'wifi' || strtolower($cat) == 'wi-fi') { ?>
                  onClick="toggleWiFi()"
                  <?php } else { ?>
                  onClick="toggleGroup('<?php echo strtolower($cat) ?>')"
                  <?php } ?>
                  <?php if ($first) { ?> 
                  checked="checked"
                  <?php 
                    $checked_cat = strtolower($cat);
                  } ?> />
              </span>
            </h5>
          </li>
<?php $first = false;
     } ?>
        </ul>
      </div>
      <div class="col-sm-12 col-md-2 merchant-search-cta">
        <?php if ($search_page_id = Config::get('themenp.merchant-search')) {
          $search_page = Page::getById($search_page_id);
          $nh = Loader::helper('navigation'); ?>
          <a href="<?php echo $nh->getCollectionURL($search_page); ?>" class="btn btn-primary">Search Merchant<br />Directory</a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php
/*
    Note - this goes in here because it's the only way to preserve block caching for this block. We can't
    set these values through the controller
*/
?>

<script type="text/javascript">
  var customIcons = {
    dining: {
      icon: '<?php echo $block_path ?>images/dining.png'
    },
    parking: {
      icon: '<?php echo $block_path ?>images/parking.png'
    },
    shopping: {
      icon: '<?php echo $block_path ?>images/shopping.png'
    },
    attractions: {
      icon: '<?php echo $block_path ?>images/attractions.png'
    },
    businesses: {
      icon: '<?php echo $block_path ?>images/businesses.png'
    }
  };
  var stylesArray = [
    {
      "featureType": "road.local",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "featureType": "road.arterial",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "featureType": "road.highway",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#005D73" }
      ]
    },{
      "featureType": "water",
      "stylers": [
        { "color": "#37a7cc" }
      ]
    },{
      "featureType": "poi.park",
      "stylers": [
        { "color": "#aad383" }
      ]
    },{
      "featureType": "landscape",
      "stylers": [
        { "color": "#d8d2c1" }
      ]
    },{
      "featureType": "poi.business",
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#00766C" }
      ]
    },{
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#00766C" }
      ]
    },{
      "featureType": "transit.line",
      "stylers": [
        { "color": "#ffffff" }
      ]
    }
  ];
  var markerGroups = {
    "dining": [],
    "parking": [],
    "shopping": [],
    "attractions": [],
    "businesses": [],
    "thisPage": []
  };
  var kmlLayer;
  var map;
  var wifiOverlay<?php echo $unique_identifier?>;
  function googleMapInit<?php echo $unique_identifier?>() {
    try{
      <?php if ($c->getCollectionTypeHandle() == 'merchant_page' && $c->getAttribute('merchant_coordinates')) { ?>
      var latlng = new google.maps.LatLng(<?php echo ($latitude + .003)?>, <?php echo $longitude?>);
      <?php } else { ?>
      var latlng = new google.maps.LatLng(<?php echo $latitude?>, <?php echo $longitude?>);
      <?php } ?>
      var mapOptions = {
          zoom: <?php echo $zoom?>,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          scrollwheel: <?php echo !!$scrollwheel ? "true" : "false"?>,
          mapTypeControl: false,
          styles: stylesArray
      };
      map = new google.maps.Map(document.getElementById('googleMapCanvas<?php echo $unique_identifier?>'), mapOptions);
      <?php // TODO: store this in a config setting or block setting and make it conditional ?>
      src = 'http://www.google.com/maps/d/kml?forcekml=1&mid=zQ1ie-Y0ktB8.k_-7RqS7k0TE';
      kmlLayer = new google.maps.KmlLayer(src, {
        suppressInfoWindows: false,
        preserveViewport: true,
        map: map
      });
      toggleParking();
      var infowindow = new google.maps.InfoWindow()
      <?php foreach ($merchants as $cat => $merch_list) {
        $cat = preg_replace("/\"/", "", strtolower($cat));
        foreach ($merch_list as $merch) { ?>
        var icon = customIcons['<?php echo $cat ?>'] || {};
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(<?php echo $merch['latitude'] ?>, <?php echo $merch['longitude'] ?>),
          map: map,
          icon: icon.icon,
          <?php if ($cat != $checked_cat) { ?>
          visible: false
          <?php } ?>
        });
        <?php if ($merch['id'] == $c->getCollectionID()) { ?>
          markerGroups['thisPage'].push(marker);
        <?php } else { ?>
          markerGroups['<?php echo $cat ?>'].push(marker);
        <?php } ?>
        var content = "<div class=\"map-infowindow\">";
        <?php if (isset($merch['ig_post'])) { ?>
        content += <?php echo $merch['ig_post']; ?>;
        <?php } ?>
        content += "<div class=\"text-center\"><h5><?php echo $merch['name'] ?></h5></div>";
        content += "<div class=\"text-center\"><p><?php echo $merch['description'] ?></p></div>";
        <?php  if (isset($merch['ig_link'])) { ?>
        content += <?php echo $merch['ig_link'] ?>;
        <?php } ?>
        content += "<div class=\"text-center\"><a href=\"<?php echo $merch['link'] ?>\" class=\"btn btn-secondary\" style=\"padding: 10px;\">Learn More</a></div></div>";
        google.maps.event.addListener(marker, 'click', (function(marker, content) {
            return function() {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            }
        })(marker, content));
      <?php } 
        }
      ?>
      var imageBounds = {
        north: 44.778211,
        south: 44.739197,
        east: -85.588431,
        west: -85.643374
      };
      wifiOverlay = new google.maps.GroundOverlay("<?php echo $block_path ?>images/wifi_overlay.png", imageBounds);
    } catch(e){
      $("#googleMapCanvas<?php echo $unique_identifier?>").replaceWith("<p>Unable to display map: "+e.message+"</p>")
    }
  }
  function toggleGroup(type) {
    for (var i = 0; i < markerGroups[type].length; i++) {
      var marker = markerGroups[type][i];
      if (!marker.getVisible()) {
        marker.setVisible(true);
      } else {
        marker.setVisible(false);
      }
    }
  }
  function toggleParking() {
    kmlLayer.setMap( kmlLayer.getMap() ? null : map );
  }
  function toggleWiFi() {
    wifiOverlay.setMap( wifiOverlay.getMap() ? null : map );
  }
  function initMerchantPage() {
    for (var key in markerGroups) {
      var type = markerGroups[key];
      for (var i = 0; i < type.length; i++) {
        type[i].setVisible(false);
      }
    }
    $(".map-filter").attr("checked", false);
  <?php if ($c->getAttribute('merchant_dtca_member')) { // If they're a member then they're already 'on the map' figuratively and literally ?>
    var thisPage = markerGroups['thisPage'][0];
    thisPage.setVisible(true);
  <?php } else { // Otherwise, add a new marker ?>
    var icon = customIcons['<?php echo strtolower($c->getAttribute('merchant_map_category')) ?>'] || {};
    var infowindow = new google.maps.InfoWindow()
    var thisPage = new google.maps.Marker({
      position: new google.maps.LatLng(<?php echo $c->getAttribute('merchant_coordinates') ?>),
      map: map,
      icon: icon.icon
    });
    var content = "<div class=\"map-infowindow\">";
    content += "<div class=\"text-center\"><h5><?php echo $c->getCollectionName() ?></h5></div>";
    content += "<div class=\"text-center\"><p><?php echo $c->getCollectionDescription() ?></p></div>";
    content += "<div class=\"text-center\"><a href=\"<?php echo $c->getCollectionPath() ?>\" class=\"btn btn-secondary\" style=\"padding: 10px;\">Learn More</a></div></div>";
    google.maps.event.addListener(thisPage, 'click', (function(marker, content) {
      return function() {
        infowindow.setContent(content);
        infowindow.open(map, marker);
      }
    })(thisPage, content));

  <?php } ?>
    google.maps.event.trigger(thisPage, 'click');
  }
  $(function() {
    var t;
    var startWhenVisible = function (){
      if ($("#googleMapCanvas<?php echo $unique_identifier?>").is(":visible")){
        window.clearInterval(t);
        googleMapInit<?php echo $unique_identifier?>();
        <?php if ($c->getCollectionTypeHandle() == 'merchant_page' && $c->getAttribute('merchant_coordinates')) { ?>
          initMerchantPage();
        <?php } ?>
        return true;
      }
      return false;
    };
    if (!startWhenVisible()){
        t = window.setInterval(function(){startWhenVisible();},100);
    }
  });
</script>

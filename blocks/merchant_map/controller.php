<?php

namespace Concrete\Package\ThemeNp\Block\MerchantMap;

use Page;
use Concrete\Core\Block\BlockController;
use Core;
use PageList;
use Package;
use \Concrete\Core\Page\Type\Type as CollectionType;
use \Concrete\Attribute\Select\Option as SelectAttributeTypeOption;
use \Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;
use \Concrete\Core\Attribute\Type as AttributeType;
use Concrete\Attribute\Select\Controller as SelectAttributeTypeController;

class Controller extends BlockController
{
  protected $btTable = 'btMerchantMap';
  protected $btInterfaceWidth = "400";
  protected $btInterfaceHeight = "320";
  protected $btDefaultSet = "multimedia";
  protected $btCacheBlockRecord = true;
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = false;
  protected $btCacheBlockOutputForRegisteredUsers = false;
  protected $list;

  public $title = "";
  public $location = "Traverse City, MI, United States";
  public $latitude = "44.7630567";
  public $longitude = "-85.62063169999999";
  public $scrollwheel = false;
  public $zoom = 14;

  /**
   * Used for localization. If we want to localize the name/description we have to include this.
   */
  public function getBlockTypeDescription()
  {
    return t("Display a Google Map with markers corresponding to addresses of merchant pages");
  }

  public function getBlockTypeName()
  {
    return t("Merchant Map");
  }

  public function validate($args)
  {
    $error = Core::make('helper/validation/error');

    if (empty($args['location']) || $args['latitude'] === '' || $args['longtitude'] === '') {
      $error->add(t('You must select a valid location.'));
    }

    if (!is_numeric($args['zoom'])) {
      $error->add(t('Please enter a zoom number from 0 to 21.'));
    }

    if ($error->has()) {
      return $error;
    }
  }

  public function registerViewAssets()
  {
    $this->requireAsset('javascript', 'jquery');
    $this->addFooterItem(
      '<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>'
    );
  }

  public function view()
  {
    $pkg = Package::getById($this->getBlockObject()->getPackageId());
    $this->set('block_path', $pkg->getRelativePath().'/blocks/'.$this->btHandle.'/');
    $this->set('unique_identifier', Core::make('helper/validation/identifier')->getString(18));
    $this->set('bID', $this->bID);
    $this->set('title', $this->title);
    $this->set('location', $this->location);
    $this->set('latitude', $this->latitude);
    $this->set('longitude', $this->longitude);
    $this->set('zoom', $this->zoom);
    $this->set('scrollwheel', $this->scrollwheel);
    // Get merchant_map_category options
    $ak = CollectionAttributeKey::getByHandle('merchant_map_category');
    $sa = new SelectAttributeTypeController(AttributeType::getByHandle('select'));
    $sa->setAttributeKey($ak);
    $values = $sa->getOptions();
    $merch_cats = array();
    foreach ($values as $v) {
      array_push($merch_cats, $v->value);
    }
    $this->set('map_cateogries', $merch_cats);

    // Get the data we need from the merchant pages
    $merchants = array();
    $mt = CollectionType::getByHandle('merchant_page');
    if (is_object($mt)) {
      $this->list = new PageList();
      $this->list->disableAutomaticSorting();
      $this->list->filterByPageTypeID($mt->getPageTypeID());
      $pages = $this->list->get();
      foreach ($pages as $page) {
        if (!$page->getAttribute('merchant_dtca_member') || !$page->getAttribute('merchant_map_category') || !$page->getAttribute('merchant_coordinates')) continue;
        $coor = explode(",", $page->getAttribute('merchant_coordinates'));
        $merch_info = array(
          'latitude' => $coor[0],
          'longitude' => $coor[1],
          'ig_post_type' => 'none',
          'ig_post' => '""',
          'name' => $page->getCollectionName(),
          'description' => htmlspecialchars($page->getCollectionDescription()),
          'link' => $page->getCollectionPath(),
          'id' => $page->getCollectionID()
        );
        if ($page->getAttribute('merchant_instagram_post_id')) {
          $post = json_decode($page->getAttribute('merchant_instagram_post_json'));
          $merch['ig_post_type'] = $post->type;
          if ($post->data->type == 'video') {
            $merch_info['ig_post'] = '"<video controls><source src=\"'.$post->data->videos->low_resolution->url.'\" type=\"video/mp4\"/></video>"';
          } else {
            $merch_info['ig_post'] = '"<img src=\"'.$post->data->images->low_resolution->url.'\" class=\"img-responsive\" />"';
          }
          $merch_info['ig_link'] = '"<a href=\"'.$post->data->link.'\" target=\"_blank\" class=\"icon-instagram\"></a>"';
        }
        $merchants['"'.$page->getAttribute('merchant_map_category').'"'][] = $merch_info;
      }
    }
    $this->set('merchants', $merchants);
  }

  public function save($data)
  {
    $data += array(
      'title' => '',
      'location' => '',
      'zoom' => -1,
      'latitude' => 0,
      'longitude' => 0,
      'width' => null,
      'width' => null,
      'scrollwheel' => 0,
    );
    $args['title']       = trim($data['title']);
    $args['location']    = trim($data['location']);
    $args['zoom']        = (intval($data['zoom']) >= 0 && intval($data['zoom']) <= 21) ? intval($data['zoom']) : 14;
    $args['latitude']    = is_numeric($data['latitude']) ? $data['latitude'] : 0;
    $args['longitude']   = is_numeric($data['longitude']) ? $data['longitude'] : 0;
    $args['width']       = $data['width'];
    $args['height']      = $data['height'];
    $args['scrollwheel'] = $data['scrollwheel'] ? 1 : 0;
    parent::save($args);
  }
}

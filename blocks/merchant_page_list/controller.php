<?php

namespace Concrete\Package\ThemeNp\Block\MerchantPageList;

use \Concrete\Core\Block\BlockController;
use Loader;
use Page;
use Core;
use PageList;
use \Concrete\Core\Attribute\Type as AttributeType;
use \Concrete\Attribute\Select\OptionList as SelectAttributeTypeOptions;
use \Concrete\Core\Page\Type\Type as CollectionType;
use \Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;
use \Concrete\Attribute\Select\Controller as SelectAttributeTypeController;

class Controller extends BlockController {

  protected $btTable = 'btMerchantList';
  protected $btDefaultSet = "navigation";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;
  protected $list;

  public function getBlockTypeDescription() {
    return t("List merchant pages based on category.");
  }

  public function getBlockTypeName() {
    return t("Merchant List");
  }

  public function on_start() {
    $this->list = new PageList();
    $merchant_type = CollectionType::getByHandle('merchant_page');
    if (is_object($merchant_type)) {
      $this->list->filterByPageTypeID($merchant_type->getPageTypeID());
    }
    if ($this->merchantCategory) {
      $this->list->filterByAttribute('merchant_category', $this->merchantCategory);
    }
    $this->list->sortByName();
    return $this->list;
  }

  public function view() {
    $nh = Loader::helper('navigation');
    $this->set('nh', $nh);
    # TODO: do something to indicate an empty list
    $this->set('pages', $this->list->getResults());
  }

  public function add() {
    $this->load_cats();
  }

  public function edit() {
    $this->load_cats();
  }

  private function load_cats() {
    $catAttr = CollectionAttributeKey::getByHandle('merchant_category');
    if (is_object($catAttr)) {
      $sa = new SelectAttributeTypeController(AttributeType::getByHandle('select'));
      $sa->setAttributeKey($catAttr);
      $values = $sa->getOptions();
      $cats = array();
      foreach ($values as $v) {
        $cats[] = $v->value;
      }
      $this->set('categories', $cats);
    }
  }
}



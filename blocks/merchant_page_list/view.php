<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( !is_array($pages)) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>

<div class="tile-page-list merchant row">

<?php if (isset($blockTitle) && $blockTitle): ?>
  <div class="col-xs-12 text-center">
    <h2><?php echo h($blockTitle)?></h2>
  </div>
<?php endif; ?>

<?php
  foreach ($pages as $page):
    $title = $th->entities($page->getCollectionName());
    $url = $nh->getLinkToCollection($page);
    $thumbnail = $page->getAttribute('merchant_feature_image');
    $description = $page->getCollectionDescription();
    $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
    $description = $th->entities($description);
    // @TODO: Fetch all other attributes
    $controller = $page->getPageController();
?>
  <div class="col-xs-12 col-sm-6 col-md-4">
    <div class="tile-container white">
      <div class="tile absolute-button">
      <?php 
      $image = '';
      if ($page->getAttribute('merchant_dtca_member') && is_object($thumbnail)) {
        $imgHelper = Loader::helper('image');
        $image = $imgHelper->getThumbnail($thumbnail, 500, 500)->src; 
      }
      else {
        $merchant_cat_class = preg_replace('~[^\\pL\d]+~u', '-', html_entity_decode($page->getAttribute('merchant_category')));
        $image = $view->getThemePath() . '/css/images/shopping_icons/' . strtolower($merchant_cat_class) . '.png';
      }
      echo '<div class="merchant-image" style="background-image: url(\''.$image.'\'); background-position: center"></div>';
      ?>
        <div>
          <h4><?php echo $title; ?></h4>
          <p>
            <?php if ($page->getAttribute('merchant_dtca_member')) {
              echo $description;
            } ?>
          </p>
        </div>
        <div>
          <ul class="merchant-details">
          <?php
            if ($address = $controller->get_address($page))
              echo sprintf("<li class=\"address\"><span>%s</span></li>", $address);
            if ($phone = $controller->get_phone($page))
              echo sprintf("<li class=\"phone\"><span>%s</span></li>", $phone);
            if ($page->getAttribute('merchant_np_member') && $controller->get_website($page))
              echo sprintf("<li class=\"website\"><span>%s</span></li>", $controller->get_website($page, true));
          ?>
          </ul>
        </div>  
        <div>
          <a href="<?php echo $url?>" class="btn btn-secondary"><?php echo isset($buttonText) ? $buttonText : 'View' ?></a>
        </div>
      </div>
    </div>
  </div> <!-- end .col-* -->
  <?php endforeach; ?>
</div> <!-- end .row -->
<?php } ?>

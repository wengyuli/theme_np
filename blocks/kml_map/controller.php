<?php

namespace Concrete\Package\ThemeNp\Block\KmlMap;

use Page;
use Concrete\Core\Block\BlockController;
use Core;
use File;
use Loader;
use BlockType;

class Controller extends BlockController {
  protected $btTable = 'btKmlMap';
  protected $btInterfaceWidth = "400";
  protected $btInterfaceHeight = "480";
  protected $btDefaultSet = "multimedia";
  protected $btCacheBlockRecord = true;
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = false;

  public $title = "";
  public $location = "Traverse City, MI, United States";
  public $latitude = "44.7630567";
  public $longitude = "-85.62063169999999";
  public $scrollwheel = false;
  public $zoom = 14;

  public function getBlockTypeDescription() {
    return t("Display a Google Map with polygon layers using link KML files at Google");
  }

  public function getBlockTypeName() {
    return t("KML Layer Map");
  }

  public function validate($args){
    $error = Core::make('helper/validation/error');
    if (!is_numeric($args['kmlFileID'])) {
      $error->add(t('Please select a KML file.'));
    }
    if ($error->has()) {
      return $error;
    }
  }

  public function registerViewAssets() {
    $this->requireAsset('javascript', 'jquery');
    $this->addFooterItem(
      '<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>'
    );
    $html = Loader::helper('html');
    $bt = BlockType::getByHandle('kml_map');
    $bPath = Core::make('helper/concrete/urls')->getBlockTypeAssetsURL($bt);
    $this->addFooterItem($html->javascript($this->getBlockPath().'geoxmlv3-custom.min.js'));
  }

  public function view() {
    $this->set('ident', Core::make('helper/validation/identifier')->getString(18));
    $this->set('bID', $this->bID);
    $this->set('title', $this->title);
    $this->set('scrollwheel', $this->scrollwheel);
    $this->set('block_path', $this->getBlockPath());
  }

  public function save($data) {
    $data += array(
       'title' => '',
       'width' => null,
       'height' => null,
       'scrollwheel' => 0,
       'kmlFileID' => null,
    );
    $args['title'] = trim($data['title']);
    $args['width'] = $data['width'];
    $args['height'] = $data['height'];
    $args['kmlFileID'] = is_numeric($data['kmlFileID']) ? $data['kmlFileID'] : null;
    $args['scrollwheel'] = $data['scrollwheel'] ? 1 : 0;
    parent::save($args);
  }

  function getFileID() {return $this->kmlFileID;}
  
  function getFileObject() {
    if($this->kmlFileID) {
      return File::getByID($this->kmlFileID);
    } else {
      return null;
    }
  }

  private function getBlockPath() {
    if (!$this->myBlockPath) {
      $bt = BlockType::getByHandle($this->btHandle);
      $this->myBlockPath = Core::make('helper/concrete/urls')->getBlockTypeAssetsURL($bt).'/';
    }
    return $this->myBlockPath;
  }

}
<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  if ($ig_user_id && $ig_access_token):
?>
<style>
  @media screen and (min-width: 480px) {
    .ig-post {
      width: <?php if ($numPosts > 0) echo (100/$numPosts) * 2 ?>%;
    }
  }
  @media screen and (min-width: 768px) {
    .ig-post {
        width: <?php  if ($numPosts > 0) echo (100/$numPosts) ?>%;
      }
  }
</style>
<div class="row instagram-feed">
	<?php
    foreach ($result as $post) { ?>
      <div class="col-xs-6 ig-post">
        <?php if ($popOut): ?>
        <img src="<?php echo $post->images->low_resolution->url; ?>" class="img-responsive popout-modal" data-toggle="modal" data-target="#<?php echo $post->id ?>"/>
        <div class="modal fade" id="<?php echo $post->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" style="background-image: url(<?php echo $post->user->profile_picture ?>);">
	@<?php echo $post->user->username ?>
                  <span><?php echo $post->user->full_name ?></span>
                </h4>
              </div>
              <div class="modal-body">
                <a href="<?php echo $post->link ?>" target=\"_blank\">
                  <?php if ($post->type == 'video') { ?>
                    <video style="width: 100%" controls>
                      <source src="<?php echo $post->videos->standard_resolution->url ?>" type="video/mp4"/>
                    </video>
                  <?php } else { ?>
                    <img src="<?php echo $post->images->standard_resolution->url; ?>" class="img-responsive" />
                  <?php } ?>
                </a>
              </div>
              <div class="modal-footer">
                <?php echo $post->caption->text ?>
              </div>
            </div>
          </div>
        </div>
        <?php else: ?>
          <img src="<?php echo $post->images->low_resolution->url; ?>" class="img-responsive" />
        <?php endif;?>
      </div>
  <?php } ?>
</div>
<?php endif; ?>



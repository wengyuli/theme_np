<?php
namespace Concrete\Package\ThemeNp\Block\InstagramFeed;

use \Concrete\Core\Block\BlockController;
use Loader;
use Config;

class Controller extends BlockController {

  protected $btTable = 'btInstagramFeed';
  protected $btDefaultSet = "social";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;
  protected $ig_user_id = null;
  protected $ig_access_token = null;

  public function getBlockTypeDescription() {
    return t("Display a feed of recent Instagram posts");
  }

  public function getBlockTypeName() {
    return t("Instagram Feed");
  }

  public function view() {
    $this->get_ids();
    $this->get_ig_user_media();
  }

  public function add() {
    $this->get_ig_user_data();
  }

  public function edit() {
    $this->get_ig_user_data();
  }

  protected function get_ids() {
    $this->ig_access_token = Config::get('themenp.instagram-access-token');
    $this->ig_user_id = Config::get('themenp.instagram-user-id');
    $this->set('ig_user_id', $this->ig_user_id);
    $this->set('ig_access_token', $this->ig_access_token);
  }

  protected function get_ig_user_media($posts = [], $next_url = null) {
    if ($this->ig_user_id && $this->ig_access_token) {
      $url = ($next_url) ? $next_url : "https://api.instagram.com/v1/users/$this->ig_user_id/media/recent/?access_token=$this->ig_access_token";
      $result = $this->fetch_results($url);
      if( $result && !empty($result->data) && is_array($result->data)){
        foreach ($result->data as $post) {
          if (count($posts) == $this->numPosts)
            break;
          if ($post->type == 'video' && !$this->showVideo)
            continue;
          else
            array_push($posts, $post);
        }
      }
      // Check that we found the requested number of posts for the given media type, if not go agiain...
      // IG API doesn't have a method for selecting by video or photo.
      //if (count($posts) < $this->numPosts)
        //$this->get_ig_user_media($posts, $result->pagination->next_url);
      //else
        $this->set('result', $posts);
    }
  }

  protected function fetch_results($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result);
  }

  protected function get_ig_user_data() {
    $ig_access_token = Config::get('themenp.instagram-access-token');
    $ig_user_id = Config::get('themenp.instagram-user-id');
    $this->set('ig_user_id', $ig_user_id);
    $this->set('ig_access_token', $ig_access_token);
    // Get user info if IDs are set
    if ($ig_access_token && $ig_user_id) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/$ig_user_id?access_token=$ig_access_token");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
      $result = curl_exec($ch);
      //error_log($result);
      curl_close($ch);
      $result = json_decode($result);
      if ($result->meta->code == 200) {
        $this->set('ig_uname', $result->data->username);
        $this->set('ig_full_name', $result->data->full_name);
        $this->set('ig_profile_pic', $result->data->profile_picture);
      }
    }
  }

}

<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php if ($ig_access_token && $ig_user_id): ?>
<style>
  #ig-profile > div {
    background-position: left center;
    background-size: 80px 80px;
    background-repeat: no-repeat;
    padding-left: 100px;
    margin-bottom: 30px;0
  }
</style>
<fieldset>
  <legend><?php echo t('Instagram Feed') ?></legend>
  <div class="form-group" id="ig-profile">
    <div style="background-image: url(<?php echo $ig_profile_pic ?>)">
      <h3><?php echo '@'.$ig_uname ?></h3>
      <p>Name: <?php echo $ig_full_name?></p>
      <p>ID: <?php echo $ig_user_id?></p>
    </div>
  </div>
  <div class="form-group">
    <?php echo $form->label('numPosts', t('Number of Posts')); ?>
    <select name="numPosts">
      <?php foreach (range(1,12) as $num) { ?>
        <option value="<?php echo $num ?>" <?php if ($num == $numPosts) echo 'selected="selected"'; ?>><?php echo $num ?></option>
      <?php } ?>
    </select>
  </div>
  <div class="form-group">
    <?php echo $form->label('showVideo', t('Include Videos')); ?>
    <select name="showVideo">
      <option value="1" <?php if ($showVideo == 1) echo 'selected="selected"'; ?>>Yes</option>
      <option value="0" <?php if ($showVideo == 0) echo 'selected="selected"'; ?>>No</option>
    </select>
  </div>
  <div class="form-group">
    <?php echo $form->label('popOut', t('Display in Pop Out on Click')); ?>
    <select name="popOut">
      <option value="1" <?php if ($popOut == 1) echo 'selected="selected"'; ?>>Yes</option>
      <option value="0" <?php if ($popOut == 0) echo 'selected="selected"'; ?>>No</option>
    </select>
  </div>
</fieldset>

<?php else: ?>

<div class="alert alert-info">
  <?php echo t('Add an Instagram API Access Token and User Name <a href="%s">in the dashboard</a> to use this block type.', URL::to('/dashboard/system/basics/np_theme_settings'))?>
</div>

<?php endif; ?>

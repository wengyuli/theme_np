<?php
namespace Concrete\Package\ThemeNp\Block\Gallery;

use \Concrete\Core\Block\BlockController;
use Loader;
use Page;

class Controller extends BlockController {

  protected $btTable = 'btGallery';
  protected $btExportTables = array('btGallery', 'btGalleryImages');
  protected $btDefaultSet = "multimedia";
  protected $btInterfaceWidth = "650";
  protected $btWrapperClass = 'ccm-ui';
  protected $btInterfaceHeight = "465";
  protected $btCacheBlockRecord = true;
  protected $btExportFileColumns = array('image', 'hoverImage');
  protected $btCacheBlockOutput = true;
  protected $btCacheBlockOutputOnPost = true;
  protected $btCacheBlockOutputForRegisteredUsers = true;

  public function getBlockTypeDescription() {
    return t("Display an image gallery");
  }

  public function getBlockTypeName() {
    return t("Gallery");
  }

  public function registerViewAssets() {
    $this->requireAsset('core/lightbox');
  }

  public function add() {
    $this->requireAsset('core/file-manager');
    $this->requireAsset('core/sitemap');
  }

  public function edit() {
    $this->add();
    $db = Loader::db();
    $query = $db->GetAll('SELECT * from btGalleryImages WHERE bID = ? ORDER BY sortOrder', array($this->bID));
    $this->set('rows', $query);
  }

  public function view() {
    $db = Loader::db();
    $query = $db->GetAll('SELECT * from btGalleryImages WHERE bID = ? ORDER BY sortOrder', array($this->bID));
    $this->set('images', $query);
  }

    public function delete() {
    $db = Loader::db();
    $db->execute('DELETE from btGalleryImages WHERE bID = ?', array($this->bID));
    parent::delete();
  }

  public function save($args) {
    $db = Loader::db();
    $db->execute('DELETE from btGalleryImages WHERE bID = ?', array($this->bID));
    $count = count($args['sortOrder']);
    $i = 0;
    parent::save($args);
    while ($i < $count) {
      $db->execute(
        'INSERT INTO btGalleryImages (bID, image, hoverImage, linkedPageCID, height, sortOrder) values(?,?,?,?,?,?)',
        array(
          $this->bID,
          intval($args['image'][$i]),
          intval($args['hoverImage'][$i]),
          intval($args['linkedPageCID'][$i]),
          $args['height'][$i],
          $args['sortOrder'][$i]
        )
      );
      $i++;
    }
  }

  public function duplicate($newBID) {
    $db = Loader::db();
    $v = array($this->bID);
    $q = 'select * from btGalleryImages where bID = ?';
    $r = $db->query($q, $v);
    foreach ($r as $row) {
      $db->execute(
        'INSERT INTO btGalleryImages (bID, image, hoverImage, linkedPageCID, height, sortOrder) values(?,?,?,?,?,?)',
        array(
          $newBID,
          $row['image'],
          $row['hoverImage'],
          $row['linkedPageCID'],
          $row['height'],
          $row['sortOrder']
        )
      );
    }
  }

}
<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

<?php if ( $c->isEditMode() && $controller->isBlockEmpty()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php } else { ?>

<div class="tile-page-list row">

  <?php if (isset($pageListTitle) && $pageListTitle): ?>
    <div class="col-xs-12">
      <h5><?php echo h($pageListTitle)?></h5>
    </div>
  <?php endif; ?>

  <?php
    $includeEntryText = false;
    if ($includeName || $includeDescription || $useButtonForLink) {
        $includeEntryText = true;
    }
    foreach ($pages as $page):
      // Prepare data for each page being listed...
      $title = $th->entities($page->getCollectionName());
      $url = ($page->getCollectionPointerExternalLink() != '') ? $page->getCollectionPointerExternalLink() : $nh->getLinkToCollection($page);
      $target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
      $target = empty($target) ? '_self' : $target;
      $description = $page->getCollectionDescription();
      $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
      $description = $th->entities($description);
      $thumbnail = false;
      if ($displayThumbnail) {
          $thumbnail = $page->getAttribute('thumbnail');
      }
      if (is_object($thumbnail) && $includeEntryText) {
          $entryClasses = 'ccm-block-page-list-page-entry-horizontal';
      }

      $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);
 ?>

        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="tile-container dark">
            <div class="tile<?php if ($useButtonForLink) { echo ' absolute-button'; } ?>">
            <?php if (is_object($thumbnail)) {
              $img = Core::make('html/image', array($thumbnail));
              $tag = $img->getTag();
              $tag->addClass('img-responsive');
              print $tag;
            } ?>

            <?php if ($includeEntryText): ?>
            
              <?php if ($includeName): ?>
              <div>
                <h4>
                  <?php if ($useButtonForLink) { ?>
                    <?php echo $title; ?>
                  <?php } else { ?>
                    <a href="<?php echo $url ?>" target="<?php echo $target ?>"><?php echo $title ?></a>
                  <?php } ?>
                </h4>
              </div>
              <?php endif; ?>

              <?php if ($includeDate): ?>
                <div class="ccm-block-page-list-date"><?php echo $date?></div>
              <?php endif; ?>

              <?php if ($includeDescription): ?>
                <div>
                  <p><?php echo $description ?></p>
                </div>
              <?php endif; ?>

              <?php if ($useButtonForLink): ?>
                <div>
                  <a href="<?php echo $url?>" class="btn btn-secondary"><?php echo $buttonLinkText?></a>
                </div>
              <?php endif; ?>

            </div>
            <?php endif; ?>
          </div>
        </div> <!-- end .col-* -->
  <?php endforeach; ?>
    </div> <!-- end .row -->


<?php if ($showPagination): ?>
    <?php echo $pagination;?>
<?php endif; ?>

<?php } ?>

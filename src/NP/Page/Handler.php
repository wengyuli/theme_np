<?php  

namespace NP\Page;
use Concrete\Core\Page\Page;
use Config;

class Handler {

  public function fetch_google_places_data(Page $page) {
    //*****************
    // TODO: cache places JSON data for 24 hours, retry on failed attempts
    // **************** 
    if ($places_id = $page->getAttribute('merchant_google_places_id')) {
      $url = 'https://maps.googleapis.com/maps/api/place/details/json?key='.Config::get('themenp.google-api-key').'&placeid='.$places_id;
      $response = $this->remote_call($url);
      $json = json_decode($response);
      if ($json->status == 'OK') {
        $page->setAttribute('merchant_google_places_json', $response);
        $page->setAttribute('merchant_coordinates', $json->result->geometry->location->lat.','.$json->result->geometry->location->lng);
      }
      else {
        $this->geo_code($page);
      }
      return true;
    }
    return true;
  }

  public function geo_code(Page $page) {
    if ($page->getAttribute('merchant_manual_coordinates') == true) {
      return true;
    }
    $address = $page->getAttribute('merchant_address');
    if (!$address) {
      return true;
    }
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&key=".Config::get('themenp.google-api-key')."&address=";
    $request = $url . urlencode($address);
    $response = $this->remote_call($request);
    $response = json_decode($response, true);
    if($response['status'] == 'OK'){
      $lat = $response["results"][0]["geometry"]["location"]["lat"];
      $long = $response["results"][0]["geometry"]["location"]["lng"];
      $page->setAttribute('merchant_coordinates', "$lat,$long");
    }
    else {
      return false;
    }
  }

  public function fetch_ig_post(Page $page) {
    if ($post_id = $page->getAttribute('merchant_instagram_post_id')) {
      $url = 'https://api.instagram.com/v1/media/shortcode/'.$post_id.'?client_id='.Config::get('themenp.instagram-client-id');
      $response = $this->remote_call($url);
      $json = json_decode($response);
      if ($json->meta->code == 200) {
        $page->setAttribute('merchant_instagram_post_json', $response);
      }
    }
    return true;
  }

  private function remote_call($url) {
    if (!$url) {
      return false; 
    }  
    $curl = curl_init();
    $opts = array();
    $opts[CURLOPT_URL] = $url;
    $opts[CURLOPT_RETURNTRANSFER] = true;
    $opts[CURLOPT_CONNECTTIMEOUT] = 10;
    $opts[CURLOPT_TIMEOUT] = 20;
    $opts[CURLOPT_RETURNTRANSFER] = true;
    
    curl_setopt_array($curl, $opts);
    $rbody = curl_exec($curl);
    curl_close($curl);    
    return $rbody;
  }

}
<?php
namespace Concrete\Package\ThemeNp;

use Core;
use Package;
use Page;
use Loader;
use Environment;

use Concrete\Core\Page\Theme\Theme;
use \Concrete\Core\Page\Template;
use \Concrete\Core\Block\BlockType\BlockType;
use \Concrete\Core\Attribute\Key\Category as AttributeKeyCategory;
use \Concrete\Core\Attribute\Set as AttributeSet;
use \Concrete\Core\Attribute\Type as AttributeType;
use \Concrete\Attribute\Select\Option as SelectAttributeTypeOption;
use \Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;
use \Concrete\Core\Page\Type\Type as CollectionType;
use \Concrete\Core\Page\Single as SinglePage;
use \NP\Page\Handler as PageHandler;

defined('C5_EXECUTE') or die(_('Access Denied'));

class Controller extends Package
{
  protected $pkgHandle = 'theme_np';
  protected $appVersionRequired = '5.7.5';
  protected $pkgVersion = '0.2.7.3';
  protected $pkgAutoloaderRegistries = array(
    'src/NP' => '\NP'
  );

  protected $merchCategories = array(
    'Antiques & Art Galleries',
    'Apparel',
    'Books, Cards & Stationery',
    'Churches',
    'Crafts, Toys & Hobbies',
    'Florists',
    'Food Specialties',
    'Gifts & Collectables',
    'Home Furnishings',
    'Jewelry',
    'Lodging',
    'Media',
    'Medical & Pharmacies',
    'Music',
    'Printers',
    'Restaurants',
    'Shoes & Accessories',
    'Spas, Salons & Fitness',
    'Specialty Shops',
    'Sporting Goods',
    'Wedding',
    'Local Government & Non-Profit Agencies',
    'Financial Services',
    'Legal Services',
    'Business Services',
    'Theater/Attraction',
  );
  protected $merchDistricts = array(
    'Front Street District',
    'Old Town District',
    'West End District',
    'Warehouse District',
  );
  protected $merchMapCategories = array(
    'Dining',
    'Parking',
    'Shopping',
    'Attractions',
    'Businesses',
  );

  public function on_start() {
    //$objEnv = Environment::get();
    //$objEnv->overrideCoreByPackage('attributes/select/controller.php', $this);
    $handler = new PageHandler();
    \Events::addListener('on_page_version_approve', function ($event) use ($handler){
      $page = $event->getPageObject();
      if ($page->getCollectionTypeHandle() == 'merchant_page')
        $handler->fetch_google_places_data($page);
        $handler->fetch_ig_post($page);
    });
  }

  public function getPackageDescription()
  {
    return t('A custom package for novumproductions.com.');
  }

  public function getPackageName()
  {
    return t('Novum Productions Package');
  }

  public function install()
  {
    $pkg = parent::install();
    Theme::add('np', $pkg);
    $this->install_custom_attributes($pkg);
    $this->install_custom_page_types($pkg);
    $this->configure_blocks($pkg);
    $this->configureSinglePages($pkg);
  }

  public function uninstall()
  {
    parent::uninstall();
  }

  public function upgrade()
  {
    $pkg = Package::getByHandle('theme_np');
    $this->install_custom_attributes($pkg);
    $this->install_custom_page_types($pkg);
    $this->configure_blocks($pkg);
    $this->configureSinglePages($pkg);

    // Make sure default template for merchant page is correct
    $merchant_type = CollectionType::getByHandle('merchant_page');
    $merchant = Template::getByHandle('merchant');
    if (is_object($merchant_type) && is_object($merchant)) {
      $merchant_type->update(
        array(
          'defaultTemplate'     => $merchant,
          'allowedTemplates'    => 'C',
          'templates'           => array($merchant)
        ), $pkg
      );
    }

    parent::upgrade();
  }

  public function install_custom_attributes($pkg)
  {
    /*** Page page type custom attributes ***/
    $eaku = AttributeKeyCategory::getByHandle('collection');

    // Header image - add a general attributes category
    $genset = AttributeSet::getByHandle('general');
    if (!is_object($genset)) {
      $genset = $eaku->addSet('general', t('General'), $pkg);
    }
    $this->make_attribute($pkg, $genset, 'image_file', 'header_img', 'Header Image', 0, 0);

    // Nav title (text area)
    $navset = AttributeSet::getByHandle('navigation'); # This should exist by default, TODO: error handling here?
    $this->make_attribute($pkg, $navset, 'textarea', 'nav_title', 'Nav Title', 0, 0);
    $this->make_attribute($pkg, $navset, 'image_file', 'nav_img', 'Nav Image', 0, 0);

    /*** Merchant page type custom attributes ****/
    $merch_set = AttributeSet::getByHandle('merchant');
    if (!is_object($merch_set)) {
      $merch_set = $eaku->addSet('merchant', t('Merchants'), $pkg);
    }
    // Sub Title
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_sub_title', 'Sub Title');
    // Address
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_address', 'Address');
    // Instagram Post ID
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_instagram_post_id', 'Instagram Post ID', 0, 0);
    $this->make_attribute($pkg, $merch_set, 'textarea', 'merchant_instagram_post_json', 'Instagram Post Data');
    // Hours
    $this->make_attribute($pkg, $merch_set, 'textarea', 'merchant_hours', 'Hours'); // TODO: May be replace with a special block
    // Website
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_website', 'Website');
    // Phone
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_phone', 'Phone');
    // Featured image
    $this->make_attribute($pkg, $merch_set, 'image_file', 'merchant_feature_image', 'Featured Image', 0, 0);
    // Category (Dining, Shopping, etc.)
    $this->make_attribute($pkg, $merch_set, 'select', 'merchant_category', 'Category');
    // Google place JSON response
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_google_places_id', 'Google Places ID');
    $this->make_attribute($pkg, $merch_set, 'textarea', 'merchant_google_places_json', 'Google Data');
    $this->make_attribute($pkg, $merch_set, 'date', 'merchant_google_refresh', 'Last Google Data Fetch');
    // DTCA Member
    $this->make_attribute($pkg, $merch_set, 'boolean', 'merchant_dtca_member', 'DTCA Member');
    // Social Media Links
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_facebook', 'Facebook Link');
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_twitter', 'Twitter Link');
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_instagram', 'Instagram Link');
    //    Add category options
    $catAttr = CollectionAttributeKey::getByHandle('merchant_category');
    if (is_object($catAttr)) {
      foreach ($this->merchCategories as $cat) {
        $option = SelectAttributeTypeOption::getByValue($cat);
        if (!is_object($option)) {
          SelectAttributeTypeOption::add($catAttr, $cat);
        }
      }
    }
    // District (Front, Wharehouse, etc.)
    $this->make_attribute($pkg, $merch_set, 'select', 'merchant_district', 'District');
    //    Add district options
    $distAttr = CollectionAttributeKey::getByHandle('merchant_district');
    if (is_object($distAttr)) {
      foreach ($this->merchDistricts as $dist) {
        $option = SelectAttributeTypeOption::getByValue($dist);
        if (!is_object($option)) {
          SelectAttributeTypeOption::add($distAttr, $dist);
        }
      }
    }
    /* Things used for maps */
    // Lat/Long
    $this->make_attribute($pkg, $merch_set, 'text', 'merchant_coordinates', 'Latitude/Longitude');
    $this->make_attribute($pkg, $merch_set, 'boolean', 'merchant_manual_coordinates', 'Manually Override Latitude/Longitude');
    // Map Category - more general category list used on map, leave blank to exclude from map
    $this->make_attribute($pkg, $merch_set, 'select', 'merchant_map_category', 'Map Category');
    //    Add district options
    $mapAttr = CollectionAttributeKey::getByHandle('merchant_map_category');
    if (is_object($mapAttr)) {
      foreach ($this->merchMapCategories as $cat) {
        $option = SelectAttributeTypeOption::getByValue($cat);
        if (!is_object($option)) {
          SelectAttributeTypeOption::add($mapAttr, $cat);
        }
      }
    }
    // Brands (maybe a special attribute type?)
    // ???
    return;
  }

  private function make_attribute($pkg, $set, $type, $handle, $name, $search = 1, $index = 1)
  {
    $cak = CollectionAttributeKey::getByHandle($handle);
    if (!is_object($cak)) {
      CollectionAttributeKey::add(
        AttributeType::getByHandle($type),
        array(
          'akHandle' => $handle,
          'akName' => t($name),
          'akIsSearchable' => $search,
          'akIsSearchableIndexed' => $index,
        ), $pkg
      )->setAttributeSet($set);
    }
  }

  public function install_custom_page_types($pkg)
  {
    /**** Set up page templates ****/
    // Full / default
    $full = Template::getByHandle('full');
    if (!is_object($full)) {
      $full = Template::add('full', 'Full', 'full.png');
    }
    // Full Wide
    $full_wide = Template::getByHandle('full_wide');
    if (!is_object($full_wide)) {
      $full_wide = Template::add('full_wide', 'Full Wide', 'full.png');
    }
    // Three Column
    $three_column = Template::getByHandle('three_column');
    if (!is_object($three_column)) {
      $three_column = Template::add('three_column', 'Three Column', 'three_column.png');
    }
    // Right Sidebar
    $right = Template::getByHandle('right_sidebar');
    if (!is_object($right)) {
      $right = Template::add('right_sidebar', 'Right Sidebar', 'right_sidebar.png');
    }
    // Empty Sidebar
    $empty_sidebar = Template::getByHandle('empty_sidebar');
    if (!is_object($empty_sidebar)) {
      $empty_sidebar = Template::add('empty_sidebar', 'Empty Sidebar', 'right_sidebar.png');
    }
    // Home
    $home = Template::getByHandle('home');
    if (!is_object($home)) {
      $home = Template::add('home', 'Home', 'full_list.png');
    }
    // Merchant
    $merchant = Template::getByHandle('merchant');
    if (!is_object($merchant)) {
      $merchant = Template::add('merchant', 'Merchant', 'right_sidebar.png');
    }
    // Event template
    $event = Template::getByHandle('pe_event');
    if (!is_object($event)) {
      $event = Template::add('pe_event', 'Event', 'right_sidebar.png');
    }

    /**** Set up page types ****/
    // Page
    $page_type = CollectionType::getByHandle('page');
    if (is_object($page_type)) { # "Page" page type should already exist by default
      error_log('Page page type found');
      $page_type->update(
        array(
          'defaultTemplate'     => $right,
          'allowedTemplates'    => 'C',
          'templates'           => array($right, $empty_sidebar, $three_column, $full, $full_wide, $home),
          'ptLaunchInComposer'  => 1,
          'ptIsFrequentlyAdded' => 1
        ), $pkg
      );
    }
    // Merchant
    $merchant_type = CollectionType::getByHandle('merchant_page');
    if (!is_object($merchant_type)) {
      $merchant_type = CollectionType::add(
        array(
          'handle'              => 'merchant_page',
          'name'                => 'Merchant Page',
          'defaultTemplate'     => $merchant,
          'allowedTemplates'    => 'C',
          'templates'           => array($right),
          'ptLaunchInComposer'  => 1,
          'ptIsFrequentlyAdded' => 0
        ), $pkg
      );
    }

    return;
  }

  public function configure_blocks($pkg)
  {
    if (!is_object(BlockType::getByHandle('cta_button'))) {
      BlockType::installBlockTypeFromPackage('cta_button', $pkg);
    }
    if (!is_object(BlockType::getByHandle('carousel'))) {
      BlockType::installBlockTypeFromPackage('carousel', $pkg);
    }
    if (!is_object(BlockType::getByHandle('instagram_feed'))) {
       BlockType::installBlockTypeFromPackage('instagram_feed', $pkg);
     }
    if (!is_object(BlockType::getByHandle('gallery'))) {
      BlockType::installBlockTypeFromPackage('gallery', $pkg);
    }
    if (!is_object(BlockType::getByHandle('tile'))) {
      BlockType::installBlockTypeFromPackage('tile', $pkg);
    }
    if (!is_object(BlockType::getByHandle('vimeo'))) {
      BlockType::installBlockTypeFromPackage('vimeo', $pkg);
    }
    if (!is_object(BlockType::getByHandle('merchant_map'))) {
      BlockType::installBlockTypeFromPackage('merchant_map', $pkg);
    }
    if (!is_object(BlockType::getByHandle('merchant_search'))) {
      BlockType::installBlockTypeFromPackage('merchant_search', $pkg);
    }
    if (!is_object(BlockType::getByHandle('merchant_page_list'))) {
      BlockType::installBlockTypeFromPackage('merchant_page_list', $pkg);
    }
    if (!is_object(BlockType::getByHandle('kml_map'))) {
      BlockType::installBlockTypeFromPackage('kml_map', $pkg);
    }
    return;
  }

  public function configureSinglePages($pkg) {
    SinglePage::add('dashboard/system/basics/np_theme_settings', $pkg);
    #SinglePage::add('dashboard/system/backup/csv_import', $pkg); # Moving to a separate package
  }
}


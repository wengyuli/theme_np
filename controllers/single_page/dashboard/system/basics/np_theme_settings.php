<?php

namespace Concrete\Package\ThemeNp\Controller\SinglePage\Dashboard\System\Basics;
use \Concrete\Core\Page\Controller\DashboardPageController;
use Config;
use Loader;
use \File;

class NPThemeSettings extends DashboardPageController {

  public function view() {
    $this->set('ig_access_token', h(Config::get('themenp.instagram-access-token')));
    $this->set('ig_user_id', h(Config::get('themenp.instagram-user-id')));
    $this->set('ig_user_name', h(Config::get('themenp.instagram-user-name')));
    $this->set('google_api_key', h(Config::get('themenp.google-api-key')));
    $this->set('merchant_search', h(Config::get('themenp.merchant-search')));
    $this->set('merchant_header_img', h(Config::get('themenp.merchant-header-img')));
  }

  public function theme_settings_saved() {
    $this->set('message', t("Your site's settings have been saved."));
    $this->view();
  }

  public function update_theme_settings() {
    if ($this->token->validate("update_theme_settings")) {
      if ($this->isPost()) {
        Config::save('themenp.instagram-access-token', $this->post('INSTAGRAM-ACCESS-TOKEN'));
        Config::save('themenp.instagram-user-id', $this->post('INSTAGRAM-USER-ID'));
        Config::save('themenp.instagram-user-name', $this->post('INSTAGRAM-USER-NAME'));
        Config::save('themenp.google-api-key', $this->post('GOOGLE-API-KEY'));
        Config::save('themenp.merchant-search', $this->post('merchant-search'));
        Config::save('themenp.merchant-header-img', $this->post('merchant-header-img'));
        $this->redirect('/dashboard/system/basics/np_theme_settings','theme_settings_saved');
      }
    } else {
      $this->error->add($this->token->getErrorMessage());
    }
    $this->view();
  }

  public function getFileID() {
    if ($file_id = Config::get('themenp.merchant-header-img')) {
      error_log('FILE ID: '.$file_id);
      return $file_id;
    }
    return false;
  }

  public function getFileObject() {
    error_log('getFileObject');
    return File::getByID($this->getFileID());
  }

}

<?php

namespace Concrete\Package\ThemeNp\Controller\SinglePage\Dashboard\System\Backup;
use Core;
use File;
use Page;
use \Concrete\Core\Page\Controller\DashboardPageController;
use Concrete\Core\File\Importer as FileImporter;
use \Concrete\Core\Page\Type\Type as CollectionType;
use \Concrete\Core\Page\PageList;
use \NP\Page\Handler as PageHandler;

class CsvImport extends DashboardPageController 
{
  protected $mode = 'create_and_update';  # TODO: add a form to set this on the single page
  protected $delimiter = ',';
  protected $enclosure = '"';
  protected $results = array();
  protected $merchMapCategories = array(
    'Antiques & Art Galleries'  => 'Shopping',
    'Apparel'                   => 'Shopping',
    'Books, Cards & Stationery' => 'Shopping',
    'Churches'                  => 'Attractions',
    'Crafts, Toys & Hobbies'    => 'Shopping',
    'Florists'                  => 'Shopping',
    'Food Specialties'          => 'Shopping',
    'Gifts & Collectables'      => 'Shopping',
    'Home Furnishings'          => 'Shopping',
    'Jewelry'                   => 'Shopping',
    'Lodging'                   => 'Businesses',
    'Media'                     => 'Businesses',
    'Medical & Pharmacies'      => 'Shopping',
    'Music'                     => 'Shopping',
    'Printers'                  => 'Shopping',
    'Restaurants'               => 'Dining',
    'Shoes & Accessories'       => 'Shopping',
    'Spas, Salons & Fitness'    => 'Businesses',
    'Specialty Shops'           => 'Shopping',
    'Sporting Goods'            => 'Shopping',
    'Wedding'                   => 'Shopping',
    'Local Government & Non-Profit Agencies' => 'Businesses',
    'Financial Services'        => 'Businesses',
    'Legal Services'            => 'Businesses',
    'Business Services'         => 'Businesses',
    'Theater/Attraction'        => 'Attractions',
  );

  public function process()
  {
    if ($this->token->validate("import")) {
      $valn = Core::make("helper/validation/numbers");
      $fi = new FileImporter();
        
      $fID = $this->post('fID');
      if (!$valn->integer($fID)) {
         $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
      } else {
        $f = File::getByID($fID);
        if (!is_object($f)) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        }
      }
        
      if (!$this->error->has()) {
        $fsr = $f->getFileResource();
        if (!$fsr->isFile()) {
          $this->error->add($fi->getErrorMessage(FileImporter::E_FILE_INVALID));
        } else {
          $this->set('message', t('File Parsed Successfully'));
          // TODO: Process file and stage data in a separate table? Or something that avoids a long running process
          //       How to identify columns and map to attributes? YAML orgnaized by page type?
          $rows = $this->parse_file($fsr);
          foreach ($rows as $row) {
            $this->create_page($row, 'merchant_page');
          }
          $this->set('results', $this->results);
        }
      }
    } else {
      $this->error->add($this->token->getErrorMessage());
    }
  }

  private function parse_file($fsr) 
  {
    $rows = $rows = array_filter(explode("\n", $fsr->read()));
    $header = null;
    $data = array();
    foreach ($rows as $row) {
      $row = str_getcsv($row, $this->delimiter);
      if(!$header)
        $header = $row;
      else
        $data[] = array_combine($header, $row);
    }
    return $data;
  }

  private function create_page($row, $type)
  {
    // Our attrbibute config - just merchant right now
    $config = array(
      'merchant_page' => array(
        'name' => 'Business Name',
        'cDescription' => 'Tagline',
        'meta_keywords' => 'Keywords',
        'merchant_address' => array('Number','Direction','Street','Address 2','City','State','Zip'),
        'merchant_phone' => 'Phone',
        'merchant_website' => 'Website',
        'merchant_category' => 'Category',
        'merchant_np_member' => 'Member',
        'merchant_district' => 'District',
        'merchant_instagram_post_id' => 'Instagram Video',
        'merchant_google_places_id' => 'Google Places ID',
        'merchant_facebook' => 'Facebook Link',
        'merchant_twitter' => 'Twitter Link',
        'merchant_instagram' => 'Instagram Link'
      )
    );
    // Translate the array into somehting where the keys match our attribute names
    $pageData = array();
    foreach ($config[$type] as $attr => $value) {
      if ($attr == 'merchant_address') {
        $address = array_values(array_filter(array_intersect_key($row, array_flip($value))));
        $address[count($address) - 3] .= ',';        
        $pageData[$attr] = implode(" ", $address);
      }
      else {
        $pageData[$attr] = $row[$value];
      }
    }
    // Get page type and parent
    $pageType = CollectionType::getByHandle('merchant_page');
    $parentPage = Page::getByPath('/merchants');

    // Look for an existing page with this name
    $list = new \Concrete\Core\Page\PageList();
    $list->ignorePermissions();
    $list->filterByName($pageData['name'], true);
    $pl_results = $list->getResults();
    if ($list->getTotalResults() > 1) {
      array_push($this->results, 'Skipped page: '.$pageData['name'].'. Multiple pages with this name where found.');
      return;
    }
    else if ($list->getTotalResults() == 1) {
      $newPage = $pl_results[0]; 
    }

    // Create or update page
    if (is_object($newPage)) {
      if ($this->mode == 'create_only')
        return;
      $newPage->update(array(
        'cDescription' => $pageData['cDescription'],
        'cDatePublic' => date('Y-m-d H:i:s')
      ));
      $action = 'Updated';
    }
    else {
      if ($this->mode == 'update_only')
        return;
      $newPage = $parentPage->add($pageType, array(
        'name' => $pageData['name'],
        'cDescription' => $pageData['cDescription'],
        'cDatePublic' => date('Y-m-d H:i:s')
      ));
      $action = 'Created';
    }

    // Set attributes
    $newPage->setAttribute('exclude_nav', true);
    foreach ($pageData as $attr_handle => $value) {
      if ($attr_handle == 'name' || $attr_handle == 'cDescription' || $value == '')
        continue;
      $newPage->setAttribute($attr_handle, $value);
    }
    if (isset($this->merchMapCategories[$pageData['merchant_category']])) {
      $newPage->setAttribute('merchant_map_category', $this->merchMapCategories[$pageData['merchant_category']]);
    }

    // Call web services
    $handler = new PageHandler();
    if ($pageData['merchant_google_places_id'])
      $handler->fetch_google_places_data($newPage);
    if ($pageData['merchant_instagram_post_id'])
    $handler->fetch_ig_post($newPage);
    
    array_push($this->results, $action.' page: '.$pageData['name']);
  }

}
<?php

namespace Concrete\Package\ThemeNp\Controller\PageType;
use Concrete\Core\Page\Controller\PageTypeController;
use Page;
use Config;
use Core;
use \File;
use \NP\Page\Handler as PageHandler;

class MerchantPage extends PageTypeController
{
  protected $photo_url = 'https://maps.googleapis.com/maps/api/place/photo';
  protected $google_json = false;

  public function view() {
    $c = Page::getCurrentPage();
    
    $date = Core::make('helper/date');
    if (!$c->getAttribute('merchant_google_refresh') || $date->getSystemDateTime($c->getAttribute('merchant_google_refresh')) < $date->getLocalDateTime('-24 hours')) {
      $handler = new PageHandler();
      $handler->fetch_google_places_data($c);
    }
    
    // Load up all the attributes we need for the merchant page view
    // Actual page attributes take precedence over those found in the Google JSON response
    $this->set('maps_api_key', Config::get('themenp.google-api-key'));
    $this->set('address', $this->get_address($c));
    $this->set('phone', $this->get_phone($c));  
    $this->set('website', $this->get_website($c));
    $this->set('dtca_member', $c->getAttribute('merchant_dtca_member'));
    $this->set('merchant_cat_class', $this->make_cat_class($c->getAttribute('merchant_category')));
    #$this->set('places_img_src', $this->get_places_img_src($c));
    $this->set('hours', $this->get_hours($c));
    $this->set('merchant_facebook', $c->getAttribute('merchant_facebook'));
    $this->set('merchant_twitter', $c->getAttribute('merchant_twitter'));
    $this->set('merchant_instagram', $c->getAttribute('merchant_instagram'));

    if ($banner = $c->getAttribute('header_img')) {
      $header_img = $banner->getRelativePath();
    }
    elseif ($banner = Config::get('themenp.merchant-header-img')) {
      $header_img = File::getByID($banner)->getRelativePath();
    }
    $this->set('header_img', $header_img);
    
    if ($c->getAttribute('merchant_coordinates')) {
      $coor = explode(",", $c->getAttribute('merchant_coordinates'));
      $this->set('latitude', $coor[0]);
      $this->set('longitude', $coor[1]);
    }
    // Get Instagram media JSON
    if ($c->getAttribute('merchant_instagram_post_json')) {
      $instagram = json_decode($c->getAttribute('merchant_instagram_post_json'));
      $this->set('instagram', $instagram);
      $this->set('instagram_post', $this->get_instagram_post($c));
    }
  }

  public function get_address($page) {
    $address = '';
    if ($page->getAttribute('merchant_address')) {
      $address = $page->getAttribute('merchant_address');
    }
    elseif ($google = $this->get_google($page)) {
      $address = preg_replace('/, United States$/', '', $google->result->formatted_address);
    }
    return $address;
  }

  public function get_phone($page) {
    $phone = '';
    if ($page->getAttribute('merchant_phone')) {
      $phone = $page->getAttribute('merchant_phone');
    }
    elseif ($google = $this->get_google($page)) {
      $phone = $google->result->formatted_phone_number;
    }
    return $phone;
  }

  public function get_website($page, $shorten = false) {
    $website = '';
    if ($page->getAttribute('merchant_website')) {
      $website = $page->getAttribute('merchant_website');
      $website = preg_replace('/^http:\/\/|^https:\/\//', '', $website);
    }
    elseif ($google = $this->get_google($page)) {
      $website = $google->result->website;
      $website = preg_replace('/^http:\/\/|^https:\/\//', '', $website);
      $website = preg_replace('/\/$/', '', $website);
    }
    if ($website) {
      $label = $website;
      if ($shorten) {
        $label = Core::make('helper/text')->shorten($website, 30);
        if (strlen($fulltext) > 30) {
          $label .= '&hellip;';
        }
      }
      $website = '<a href="http://'.$website.'" target="_blank">'.$label.'</a>';
    }
    return $website;
  }

  public function get_instagram_post($page) {
    if ($post = $page->getAttribute('merchant_instagram_post_json')) {
      $post = json_decode($page->getAttribute('merchant_instagram_post_json'));
      if ($post->data->type == 'video') {
        return '<video controls class="img-responsive"><source src="'.$post->data->videos->low_resolution->url.'" type="video/mp4"/></video>';
      } else {
        return '<img src="'.$post->data->images->low_resolution->url.'" class="img-responsive" />';
      }
      return '';
    }
  }

  public function get_places_img_src($page) {
    $img_src = '';
    if ($google = $this->get_google($page)) {
      if ($photo_ref = $google->result->photos[0]->photo_reference) {
        $img_src = $this->photo_url . '?key=' . Config::get('themenp.google-api-key') . '&maxwidth=500&photoreference=' . $photo_ref;
      }
    }
    return $img_src;
  }

  public function get_hours($page) {
    $hours = '';
    if ($google = $this->get_google($page))
      $hours = $google->result->opening_hours->weekday_text;
    return $hours;
  }

  private function get_google($page) {
    if ($this->google_json != false)
      return $this->google_json;
    if ($page->getAttribute('merchant_google_places_json')) {
      $this->google_json = json_decode($page->getAttribute('merchant_google_places_json'));
    }
    return $this->google_json;
  }

  private function make_cat_class($cat) {
    $cat_class = preg_replace('~[^\\pL\d]+~u', '-', html_entity_decode($cat));
    return strtolower($cat_class);
  }


}